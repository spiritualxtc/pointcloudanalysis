#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec4 pos;

void main() 
{

  float r = mod(pos.x, 1.0f);
  float g = mod(pos.y, 1.0f);
  float b = mod(pos.z, 1.0f);

  //color image int BGRA Color format
  //gl_FragColor = vec4(r, g, b, 1.0f);
  gl_FragColor = vec4(1, 1, 1, 1.0f);
}