__________      .__        __    _________ .__                   .___    _____                .__               .__        
\______   \____ |__| _____/  |_  \_   ___ \|  |   ____  __ __  __| _/   /  _  \   ____ _____  |  | ___.__. _____|__| ______
 |     ___/  _ \|  |/    \   __\ /    \  \/|  |  /  _ \|  |  \/ __ |   /  /_\  \ /    \\__  \ |  |<   |  |/  ___/  |/  ___/
 |    |  (  <_> )  |   |  \  |   \     \___|  |_(  <_> )  |  / /_/ |  /    |    \   |  \/ __ \|  |_\___  |\___ \|  |\___ \ 
 |____|   \____/|__|___|  /__|    \______  /____/\____/|____/\____ |  \____|__  /___|  (____  /____/ ____/____  >__/____  >
                        \/               \/                       \/          \/     \/     \/     \/         \/        \/ 

Information:
------------
Point Cloud Analysis - Computer Vision for Robotics 
Copyright (C) 2015  PCA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.




Libraries:
----------
- JRE 1.8

- OpenCV 3.0
- Processing API 2.2.1
- KinectPV2 0.7.5
- Toxiclibs 2.0

- OpenNI 2.1 *
- SensorKinect **

- Microsoft Kinect for Windows SDK 2.0 
    (Required for KinectPV2)

* Not in use.
** Buggy as hell. Doesn't support enough Kinect functionality required for the project.

Libraries are all prepackaged, except for MS Kinect
Download: https://www.microsoft.com/en-us/download/details.aspx?id=44561


NOTES:
------

OpenNI:
 - OpenNI is disabled from use.
    The initialisation code is still present [org.pca.model.openni] however the Kinect v2 driver had too many issues that needed to be solved before progress could be made.
         
 - Adapters:
    Were only used to access OpenNI functionality.         
     
         
INPUT:
------
Mouse Controls:
 - Left Mouse + Movement:
    Rotate/Arc Camera
 - Right Mouse + Movement:
    Zoom
 - Middle Mouse + Movement:
    Pan Camera
 - Middle Mouse + Movement + SHIFT
    Move Camera forward / backward
    
Keyboard Controls:
 - R:
    Reset Camera
 - Shift:
    Moves the Camera forwards/backwards instead of panning
    
   
BUGS:
-----
3D Bound box:
    ISSUE:
    - Bound Box is extruding to the background
    - A select few points are still being flagged as selected, when they are in the background.
    
    SOLUTION:
    - Still under investigation
    
    POTENTIAL SOLUTION:
    - Voxel Lookup 
         
User Input:
    ISSUE:
    - Keyboard Controls are "sticky".
    - When a key is pressed, it stays pressed, until another key is pressed.
    
    SOLUTION:
    - Use event callbacks on Processing to track keyboard input rather than the basic last key pressed currently in use.