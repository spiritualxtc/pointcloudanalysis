/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model;

import java.nio.FloatBuffer;

import processing.core.PVector;

/**
 * Abstract interface for a device
 * @author Spirit
 *
 */
public interface IDevice 
{
	/**
	 * Determine whether the device is open.
	 * @return Open State of the device
	 */
	boolean isOpen();
	
	/**
	 * 
	 * @return
	 */
	int[] getRawColorData();
	
	/**
	 * Gets the Width of the Color Stream
	 * @return
	 */
	int getColorWidth();
	
	/**
	 * Gets the Height of the Color Stream
	 * @return
	 */
	int getColorHeight();
	
	/**
	 * Gets the Width of the Depth Stream
	 * @return
	 */
	int getDepthWidth();
	
	/**
	 * Gets the Height of the Depth Stream
	 * @return
	 */
	int getDepthHeight();
	
	/**
	 * Gets the Color Channel Data
	 * @return A FloatBuffer with RGB values
	 */
	FloatBuffer getColorChannelData();
	
	/**
	 * Gets the Point Cloud Data that maps to the Color Channel Data
	 * @return
	 */
	FloatBuffer getPointCloudColorPosition();
	
	/**
	 * Gets the Point Cloud Data
	 * @return
	 */
	FloatBuffer getPointCloudDepthPosition();
	
	/**
	 * Maps a coordinate in 3D Camera Space to the color image.
	 * @param camera
	 * @return
	 */
	PVector MapCameraPointToColorSpace(PVector camera);
}
