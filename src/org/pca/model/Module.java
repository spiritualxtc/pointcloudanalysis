/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.pca.model.utils.FPS;
import org.pca.view.layer.Layer;

/**
 * Modules are for all intensive purposes is....
 * A controller that interacts with specific set of Data in the Model
 * and the overlay displayed in the ProcessingComponent.
 * 
 * @author Spirit
 *
 */
public abstract class Module implements IModule, Runnable 
{
	public static final int DEFAULT_DELAY = 1000;
	
	private String _id;
	private String _name;
	
	private boolean _running = false;
	private FPS _fps = null;
	private int _delay = 1000;
	
	// Sensor device
	private IDevice _device = null;
	
	// Collection of layers to draw to the screen
	private List<Layer> _layers;
	
	
	/**
	 * Gets the Module ID
	 */
	@Override
	public String getID() {return _id;}
	
	/**
	 * Gets the Modules "Nice" Name
	 */
	@Override
	public String getName() {return _name;}
	
	/**
	 * Get collection of layers used by processing
	 */
	@Override
	public Collection<Layer> getLayers() {return Collections.unmodifiableCollection(_layers);}
	
	/**
	 * Gets the FPS object
	 * @return
	 */
	public FPS getFPS() {return _fps;}
	
	/**
	 * Gets the Device Handle
	 * @return
	 */
	public IDevice getDevice() {return _device;}
	
	/**
	 * Create a module using the default update rate [once every second]
	 * @param device
	 */
	public Module(IDevice device, String id, String name)
	{
		this(device, id, name, DEFAULT_DELAY);
	}
	
	/**
	 * Create a module using a custom update rate
	 * @param device
	 * @param delay
	 */
	public Module(IDevice device, String id, String name, int delay)
	{
		_id = id;
		_name = name;		
		_device = device;
		_delay = delay;
		
		// Create Layer List
		_layers = new ArrayList<>();
		
	
	}
	
	/**
	 * Adds a layer
	 * @param layer
	 */
	protected void addLayer(Layer layer)
	{
		_layers.add(layer);
	}
	
	/**
	 * Starts the Module
	 */
	@Override
	public void start()
	{	
		_running = true;
		new Thread(this).start();
	}

	/**
	 * Stops the Module
	 */
	@Override
	public void stop()
	{
		_running = false;
	}
		
	/**
	 * Run the Thread
	 */
	@Override
	public void run()
	{
		System.out.println("Thread Module Started");
		
		_fps = new FPS();
		
		// Execute hte module for as long as it's running
		while (_running)
		{
			// Update timing
			_fps.update();
					
			try
			{
				// Update the frame
				onProcessFrame(_fps.getFrameTime());
				
				// Sleep for specified time
				Thread.sleep(_delay);
			}
			catch (Exception e)
			{
				System.out.println("THREAD: Exception Occured");

				// Will probably pass the exception down the pipeline
				e.printStackTrace();
				
				_running = false;
			}
		}
		
		System.out.println("Thread Module Stopped");	
	}

	/**
	 * Process real-time module updates
	 * 
	 * @param timeDelta
	 *            the time between calls to the method in seconds
	 */
	public abstract void onProcessFrame(float timeDelta);
}
