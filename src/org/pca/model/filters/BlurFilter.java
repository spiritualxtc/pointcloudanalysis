/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Blurs the input image with surrounding pixels.
 * @author Spirit
 *
 */
public class BlurFilter extends MatFilter 
{
	private Mat _image = null;
	
	private double _blur = 3;
	
	@Override
	public Mat getImage() {return _image;}
	
	
	/**
	 * Creates a blur filter
	 * @param blur Blur Amount
	 */
	public BlurFilter(double blur)
	{
		_blur = blur;
	}
	
	@Override
	protected Mat doFilter(Mat input)
	{
		// Create the Image Matrix
		if (_image == null || _image.cols() != input.width() || _image.rows() != input.height())
		{
			_image = new Mat(input.height(), input.width(), input.type());
		}	
		
		// Blur
		Imgproc.blur(input, _image, new Size(_blur, _blur));

		return _image;
	}

}
