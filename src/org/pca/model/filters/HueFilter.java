/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * A filter which isolates a target hue and spread around it. It returns a Mat If a pixel is isolated, Input the hue and
 * range around the hue to be selected.
 */
public class HueFilter extends MatFilter
{
	public static final int	RED		= 0;
	public static final int	YELLOW	= 43;
	public static final int	GREEN	= 85;
	public static final int	CYAN	= 127;
	public static final int	BLUE	= 170;
	public static final int	PURPLE	= 213;

	private Mat	_result		= new Mat();
	private int	_targetHue	= 0;
	private int	_spread		= (int) ((1 / 6f) * 255 / 2f);	// percentage of hue range to include

	static final int	SAT_MIN	= 50;
	static final int	VAL_MIN	= 25;

	@Override
	public Mat getImage()
	{
		return _result;
	}

	/**
	 * 
	 */
	public HueFilter()
	{
	}

	/**
	 * 
	 * @param hue
	 *            to isolate. must be hue >= 0 && hue < 256)
	 * @param spread
	 *            how far to spread around the colour wheel, 0f is none, 1f is all.
	 */
	public HueFilter(int hue, float spread)
	{
		assert(hue >= 0 && hue < 256);
		_targetHue = hue;
		_spread = (int) ((spread / 2) * 255);
	}

	/**
	 * @return an Mat image mask where white pixels represent filtered pixels and black represents discarded pixels
	 */
	@Override
	protected Mat doFilter(Mat input)
	{
		assert(input.type() == CvType.CV_8UC3);

		// Ensure enough space is allocated for resulting image
		_result.create(input.size(), input.type());

		/*
		 * The following algorithm copies data out of the native object into java 
		 */

		
		input.copyTo(_result);
		Imgproc.cvtColor(_result, _result, Imgproc.COLOR_BGR2HSV_FULL);
		List<Mat> chan = new ArrayList<Mat>(3);
		Core.split(_result, chan);

		// Pull data into byte buffers so it can be manipulated
		int size = input.cols() * input.rows();
		byte[] hues = new byte[size];
		byte[] sats = new byte[size];
		byte[] vals = new byte[size];
		chan.get(0).get(0, 0, hues);
		chan.get(1).get(0, 0, sats);
		chan.get(2).get(0, 0, vals);

		for (int i = 0; i < size; ++i)
		{
			// Shift desired hue to middle of byte range (0)
			int shiftedHue = Byte.toUnsignedInt(hues[i]) - _targetHue;

			// Check if hue is within spread
			if (shiftedHue >= -_spread && shiftedHue <= _spread && 
				Byte.toUnsignedInt(sats[i]) >= SAT_MIN	&& 
				Byte.toUnsignedInt(vals[i]) >= VAL_MIN)
			{
				// Set white
				sats[i] = (byte) 0; // no saturation
				vals[i] = (byte) 255; // full brightness

				// System.out.println("Before: (" + pixel[0] + "," + pixel[1] + "," + pixel[2] + ")");
				// System.out.println("After: (" + bytePixel[0] + "," + bytePixel[1] + "," + bytePixel[2] + ")");
			}
			else
			{
				// Set black
				vals[i] = (byte) 0; // no brightness
			}
		}

		// Put all the date back in
		chan.get(0).put(0, 0, hues);
		chan.get(1).put(0, 0, sats);
		chan.get(2).put(0, 0, vals);

		Core.merge(chan, _result);
		Imgproc.cvtColor(_result, _result, Imgproc.COLOR_HSV2BGR_FULL);

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		return _result;
	}
}
