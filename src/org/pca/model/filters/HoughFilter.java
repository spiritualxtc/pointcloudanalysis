/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.pca.model.utils.Line;

/**
 * Builds a Hough Line Transformation from a Binary Input Image
 * @author Spirit
 *
 */
public class HoughFilter extends Filter<Mat, Collection<Line>>
{
	private Mat _image = null;
		
	@Override
	protected Collection<Line> doFilter(Mat input) 
	{
		// Create the Image Matrix
		if (_image == null || _image.cols() != input.width() || _image.rows() != input.height())
		{
			_image = new Mat(input.height(), input.width(), input.type());
		}	
		
		// Hough Lines
		Imgproc.HoughLinesP(input, _image, 1, Math.PI/180, 100);
		
		// Build a Collection of Lines
		ArrayList<Line> lines = new ArrayList<>();
		
		for (int y = 0; y < _image.rows(); y++)
		{
			double[] vec = _image.get(y, 0);
		   
			lines.add(new Line((float)vec[0], (float)vec[1], (float)vec[2], (float)vec[3]));
		}		
		
		
		return Collections.unmodifiableCollection(lines);
	}

}
