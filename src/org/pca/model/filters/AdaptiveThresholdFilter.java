/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Filters the input image in grids and outputs a binary image.
 * Average - Constant > 0 = White
 * Average - Constant < 0 = Black
 * @author Spirit
 *
 */
public class AdaptiveThresholdFilter extends MatFilter
{
	private Mat _image = null;
	
	private int _blockSize = 11;
	private double _constant = 0;
	
	@Override
	public Mat getImage() {return _image;}
	
	
	/**
	 * Creates the adapative threshold filter using BlockSize = 11 and a constant of 0
	 */
	public AdaptiveThresholdFilter()
	{
		this(11, 0);
	}
	
	/**
	 * Creates the adapative threshold filter
	 * @param blockSize NeighborHood Area
	 * @param c Subtracts from the average
	 */
	public AdaptiveThresholdFilter(int blockSize, double c)
	{
		_blockSize = blockSize;
		_constant = c;
	}
	
	@Override
	protected Mat doFilter(Mat input)
	{
		// Create the Image Matrix
		if (_image == null || _image.cols() != input.width() || _image.rows() != input.height())
		{
			_image = new Mat(input.height(), input.width(), input.type());
		}	
	
		// Adaptive Threshold
		Imgproc.adaptiveThreshold(input, _image, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, _blockSize, _constant);
		
		return _image;
	}
}
