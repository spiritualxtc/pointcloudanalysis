/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.objdetect.CascadeClassifier;

/**
 * Processes the input image by doing image recognition. Outputs the rectangles that represent the regions of interest
 * @author Spirit
 *
 */
public class RecognitionFilter extends Filter<Mat, MatOfRect> 
{
	private CascadeClassifier 	_detector = null;
	private MatOfRect			_detections	= null;
	
	/**
	 * Get the Detected Regions
	 * @return
	 */
	public MatOfRect	getDetections() {return _detections;}
	
	/**
	 * @param cascadeClassifier
	 *            must not be null. It should be a local path from executable to
	 *            an .xml cascadeClassifier file which defines the object you
	 *            wish to detect.
	 */
	public RecognitionFilter(String cascadeClassifier)
	{
		// Load Object Descriptor File(s)
		_detector = new CascadeClassifier(cascadeClassifier);
		if (_detector == null || _detector.empty())
		{
			System.err.println("Could not use cascade classifier \"" + cascadeClassifier + "\"");
		}
		else
		{
			System.out.println("Loaded object descriptor \"" + cascadeClassifier + "\"");
		}
		
		_detections = new MatOfRect();
	}

	@Override
	protected MatOfRect doFilter(Mat input)
	{
		// Do image Recognition
		if (! _detector.empty())
			_detector.detectMultiScale(input, _detections);

		return _detections;
	}
	
}
