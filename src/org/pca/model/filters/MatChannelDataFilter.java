/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;

/**
 * Converts an OpenCV Mat to it's Channel Data
 * @author Spirit
 *
 */
public class MatChannelDataFilter extends Filter<Mat, byte[]> 
{
	private byte[] _channelData = null;

	/**
	 * Gets the channel data
	 * @return
	 */
	public byte[] getChannelData() {return _channelData;}
	
	@Override
	protected byte[] doFilter(Mat input) 
	{
		// Create Channel Data
		if (_channelData == null || (_channelData.length != input.cols() * input.rows() * input.channels()))
		{
			_channelData = new byte[input.rows() * input.cols() * input.channels()];
			
			System.out.println("Create Channel Data: Channels = " + input.channels());
		}
		
		// Get Data from Image matrix		
		input.get(0, 0, _channelData);
	
		return _channelData;
	}
	

}
