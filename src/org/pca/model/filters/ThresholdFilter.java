/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Generates an image from the input image, using a threshold and a procedure.
 * @author spirit
 *
 */
public class ThresholdFilter extends MatFilter 
{
	private Mat _image = null;
	
	private int _threshold = 127;
	private int _procedure = Imgproc.THRESH_BINARY;
	
	@Override
	public Mat getImage() {return _image;}
	
	/**
	 * Creates the Threshold Filter with default threshold of 127
	 */
	public ThresholdFilter()
	{
		this(127, Imgproc.THRESH_BINARY);
	}
	
	/**
	 * Creates the Threshold Filter
	 * @param threshold Threshold value to compare too
	 */
	public ThresholdFilter(int threshold)
	{
		this(threshold, Imgproc.THRESH_BINARY);
	}
	
	/**
	 * 
	 * @param threshold Threshold value to compare too
	 * pixel > threshold = white
	 * pixel < threshold = black
	 * @param procedure The procedure to use for generating the output image
	 * Possibilities: Imgproc.THRESH_BINARY, Imgproc.THRESH_BINARY_INV, Imgproc.THRESH_TRUNC, Imgproc.THRESH_TOZERO, Imgproc.THRESH_TOZERO_INV
	 */
	public ThresholdFilter(int threshold, int procedure)
	{
		_threshold = threshold;
		_procedure = procedure;
		
	}
	

	@Override
	protected Mat doFilter(Mat input)
	{
		// Create the Image Matrix
		if (_image == null || _image.cols() != input.width() || _image.rows() != input.height())
		{
			_image = new Mat(input.height(), input.width(), input.type());
		}	
		
		// Canny Edge Detection
		Imgproc.threshold(input, _image, _threshold, 255, _procedure);
		
		return _image;
	}
}
