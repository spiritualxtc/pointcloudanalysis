/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import java.util.ArrayList;
import java.util.Collection;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;


/**
 * Generates a list of Points that represent a contour from the input image
 * @author spirit
 *
 */
public class ContourFilter extends Filter<Mat, Collection<MatOfPoint>> 
{

	@Override
	protected Collection<MatOfPoint> doFilter(Mat input) 
	{
		// Output List
		ArrayList<MatOfPoint> mats = new ArrayList<>();
		
		// Create Heirarchy Mat
		Mat m = new Mat();
		
		// Find Contours
		try
		{
			Imgproc.findContours(input, mats, m, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
		}
		catch(Exception e)
		{
			System.out.println("Contour Exception Occurred... recovering");
			e.printStackTrace();
		}
		
		
		return mats;
	}
	

}
