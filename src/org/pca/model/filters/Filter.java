/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import java.util.ArrayList;

/**
 * Chainable filter base class
 * @author Spirit
 *
 * @param <INPUT> The input needed for this filter to process
 * @param <OUTPUT> The output child filters can use.
 */
public abstract class Filter<INPUT, OUTPUT> implements IFilter<INPUT>
{
	private ArrayList<IFilter<OUTPUT>> _filters;

	/**
	 * Creates the Filter
	 */
	public Filter()
	{
		_filters = new ArrayList<>();
	}
	

	/**
	 * Adds a child Filter. 
	 * The Input parameter of the child filter must match the output parameter of the parent filter
	 * @param filter The child Filter
	 */
	public void addFilter(IFilter<OUTPUT> filter)
	{
		_filters.add(filter);
	}
	
	
	/**
	 * Processes the filter, and passes the output to all it's child filters
	 * @param input
	 */
	public void filter(INPUT input)
	{
		// Validation
		if (input == null)
		{
			System.out.println("Filter: No Input");
			return;
		}
		
		// Apply the filter to the INPUT
//		long filterStart = System.currentTimeMillis();
		OUTPUT output = doFilter(input); 
//		System.out.println(	this.toString() + ": " + (System.currentTimeMillis() - filterStart)/1000f + "s ");
		
		// Pass the OUTPUT of the Filter as INPUT to child filters
		for (IFilter<OUTPUT> f : _filters)
			f.filter(output);
	}
	
	
	/**
	 * Filter Process
	 * @param input
	 * @return
	 */
	protected abstract OUTPUT doFilter(INPUT input);
}
