/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * Converts input channel data to an OpenCV Mat
 * @author Spirit
 *
 */
public class MatBGRRootFilter extends Filter<int[], Mat> 
{
	private int CHANNEL_COUNT = 3;
	
	private Mat _image = null;
	private byte[] _channelData = null;

	private int _width = 0;
	private int _height = 0;
	
	/**
	 * Size of the Resulting Image
	 * @param width Width of the expected image
	 * @param height Height of the expected image
	 */
	public MatBGRRootFilter(int width, int height)
	{
		_width= width;
		_height = height;
		
		// Create Image
		_image = new Mat(_height, _width, CvType.CV_8UC3);
		_channelData = new byte[_width * _height * CHANNEL_COUNT];
	}
	
	@Override
	protected Mat doFilter(int[] input)
	{
		// Validation
		if (input.length != _width * _height)
		{
			System.out.println("Invalid: ");
			return null;
		}
		
		// Copy : probs could be optimized.
		// #javaisslow
		int pos = 0;
      
		for (int i=0; i<input.length; ++i)
		{
			int r = (0xFF0000 & input[i]) >> 16;
			int g = (0x00FF00 & input[i]) >> 8;
			int b = (0x0000FF & input[i]) >> 0;
			
			_channelData[pos++] = (byte)b;
			_channelData[pos++] = (byte)g;
			_channelData[pos++] = (byte)r;
		}
		
      	// Copy Image Data
		_image.put(0, 0, _channelData);		
	
		return _image;
	}

}
