/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Uses the CannyEdge detection process on the input image and outputs a new image
 * @author Spirit
 *
 */
public class CannyEdgeFilter extends MatFilter 
{
	private Mat _image = null;
	
	private double _threshold1 = 40;
	private double _threshold2 = 120;
	
	/**
	 * Creates the CannyEdge Filter using lower threshold value of 40
	 */
	public CannyEdgeFilter()
	{
		this(40);
	}
	
	
	/**
	 * Creates the CannyEdge Filter 
	 * @param threshold Lower threshold value. Defaults the Higher threshold value to be 3x the lower
	 */
	public CannyEdgeFilter(double threshold)
	{
		this(threshold, threshold * 3);
	}
	
	/**
	 * Creates the CannyEdge Filter
	 * @param threshold1 Lower Threshold
	 * @param threshold2 Upper Threshold: Recommended to be between 2-3 times higher than threshold1
	 */
	public CannyEdgeFilter(double threshold1, double threshold2)
	{
		_threshold1 = threshold1;
		_threshold2 = threshold2;
	}
	
	
	
	
	@Override
	public Mat getImage() {return _image;}
	
	@Override
	protected Mat doFilter(Mat input)
	{
		// Create the Image Matrix
		if (_image == null || _image.cols() != input.width() || _image.rows() != input.height())
		{
			_image = new Mat(input.height(), input.width(), input.type());
		}	
			
		// Canny Edge Detection
		Imgproc.Canny(input, _image, _threshold1, _threshold2);
		
		return _image;
	}

}
