/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.voxels;

import java.util.Random;

import toxi.geom.Vec3D;

/**
 *	Implementation of Cell which adds colour value information 
 */
public class CellXYZVal extends Cell
{
	public float val; // shade of cell

	/**
	 * Standard constructor uses default xyz coord and generates a random color.
	 */
	public CellXYZVal()
	{
		super();
		createRandomVal();
	}

	/**
	 * Create Cell with specified xyz coord. Generates a random color.
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 */
	public CellXYZVal(float x, float y, float z)
	{
		super(x, y, z);
		createRandomVal();
	}

	/**
	 * Create Cell with specified xyz coord and rgb color. The rgb colour is converted into a luminance value.
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 * @param r red color between 0-1f
	 * @param g green color between 0-1f
	 * @param b blue color between 0-1f
	 */
	public CellXYZVal(float x, float y, float z, float r, float g, float b)
	{
		super(x, y, z);
		setColour(r, g, b);
	}

	/**
	 * Generate a random value
	 */
	private void createRandomVal()
	{
		Random r = new Random();
		val = r.nextFloat();
	}

	@Override
	public String toString()
	{
		return "<CellVal> pos: " + super.toString() + " value: " + val;
	}

	@Override
	public void setColour(float r, float g, float b)
	{
		// calculates approx luminance from RGB
		val = (0.2126f * r + 0.7152f * g + 0.0722f * b); 
	}

	@Override
	public Vec3D getColour()
	{
		return new Vec3D(val, val, val);
	}
}
