/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.voxels;

import toxi.geom.Vec3D;

/**
 * Cell is used by the VoxelOctree to hold information at a point in space. 
 * Extend it to add new data/methods to the Cell as required.
 */
public abstract class Cell extends Vec3D
{
	/**
	 * Creates a Cell class with default XYZ coordinates of Vec3D.
	 */
	public Cell()
	{
		super();
	}

	/**
	 * Creates a Cell containing x,y,z coords.
	 * 
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 */
	public Cell(float x, float y, float z)
	{
		super(x, y, z);
	}

	/**
	 * Creates a Cell containing x,y,z,r,g,b coords.
	 * 
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 * @param r color
	 * @param g color
	 * @param b color
	 */
	public Cell(float x, float y, float z, float r, float g, float b)
	{
		super(x, y, z);
	}

	/**
	 * Override the x,y,z coord of the cell.
	 * 
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 */
	public void setPosition(float x, float y, float z)
	{
		super.set(x, y, z);
	}

	/**
	 * Set the colour of the cell
	 * @param r red channel between 0-1f
	 * @param g green channel between 0-1f
	 * @param b blue channel between 0-1f
	 */
	abstract public void setColour(float r, float g, float b);

	/**
	 * Returns the color of the cell
	 * @return Vec3D containing color. Color values are between 0-1f.
	 */
	abstract public Vec3D getColour();

}
