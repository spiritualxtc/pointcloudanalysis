/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.voxels;

import java.nio.FloatBuffer;
import java.util.List;

import toxi.geom.AABB;
import toxi.geom.Vec3D;

public class VoxelOctreeXYZRGB extends VoxelOctree<CellXYZRGB>
{

	public VoxelOctreeXYZRGB(Vec3D o, float size)
	{
		super(o, size);
	}
	
	/**
	 * 
	 * @param vertices
	 * @param colors
	 * @param skipInterval the number of points to skip
	 */
	public void insertPoints(FloatBuffer vertices, FloatBuffer colors, int skipInterval)
	{
		if (skipInterval <= 0) { throw new IllegalArgumentException("skipInterval must be a positive integer");	}
		assert(vertices.remaining() == colors.remaining());

		float scale = 1/255f; // scale colour from 0-255 to 0-1
		
//		long startTime = System.currentTimeMillis();
		
		int pointIndex = 0;
		int randOffset = 0;
		for (int i = 0; i+2 < vertices.remaining(); i = 3 * (pointIndex + randOffset))
		{
			this.addPoint(new CellXYZRGB(
					vertices.get(i), vertices.get(i+1), vertices.get(i+2), 
					// Converting from BGR -> RGB order
					colors.get(i+2)*scale,  colors.get(i+1)*scale,  colors.get(i)*scale)); 
			
			
//			randOffset =  (int)(Math.random() * skipInterval) + skipInterval / 2;
			
			pointIndex += skipInterval;
			randOffset = (int) (Math.random() * (skipInterval - 1));
		}
		
//		System.out.println("Time to push voxels: " + ((System.currentTimeMillis() - startTime)/1000f) + "s");
		
		vertices.rewind();
		colors.rewind();
	}
	
	/**
	 * 
	 * @param aabb defines a subsection of the 
	 * @return new octree, may be null!
	 */
	public VoxelOctreeXYZRGB copyVoxels(AABB aabb)
	{
		VoxelOctreeXYZRGB newOctree = null;
		List<CellXYZRGB> cells = this.getPointsWithinBox(aabb);

		if (cells != null)
		{
			// Find the longest side length of aabb
			Vec3D extents = aabb.getExtent();
			float longestSide = 2 * Math.max(extents.z, Math.max(extents.x, extents.y));
			
			// Create new voxelOctree to represent aabb selection
			newOctree = new VoxelOctreeXYZRGB(aabb.getMin(), longestSide);
			newOctree.setMinNodeSize(this.getMinNodeSize());
			for (CellXYZRGB cell : cells)
			{
				newOctree.addPoint(cell);
			}
		}
		
		return newOctree;
	}
}
