/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.voxels;

import java.util.List;

import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * Utility extends the default octree class in order to visualize currently occupied cells in the tree.
 * 
 * @param <T> is the type contained within the cell
 */
public class VoxelOctreeRenderer<T extends Cell> extends VoxelOctree<T>
{

	public VoxelOctreeRenderer(Vec3D o, float d)
	{
		super(o, d);
	}

	public void draw(PGraphics gfx, boolean drawOctree, boolean drawVoxels)
	{
		drawNode(this, gfx, drawOctree, drawVoxels);
	}

	private void drawNode(VoxelOctree<T> vox, PGraphics gfx, boolean drawOctree, boolean drawVoxels)
	{

		// Draw wire box for octree framework
		if (!vox.isLeafNode())
		{
			if (drawOctree)
			{
				// Draw current node
				gfx.noFill();
				gfx.stroke(20, 20, 255, 10); 

				gfx.pushMatrix();
				gfx.translate(vox.x, vox.y, vox.z);
				gfx.box(vox.getNodeSize());
				gfx.popMatrix();
			}

			// Draw Children
			VoxelOctree<T>[] childNodes = vox.getChildren();
			for (int i = 0; i < 8; i++)
			{
				if (childNodes[i] != null) drawNode(childNodes[i], gfx, drawOctree, drawVoxels);
			}
		}
		// Draw voxel
		else if (drawVoxels && vox.getPoints() != null)
		{
			gfx.noStroke();

			// Colour the box
			List<T> points = vox.getPoints();
			if (points.size() > 0)
			{
				Vec3D colour = points.get(0).getColour().scale(255); // using first in cell's colour
				gfx.fill(colour.x, colour.y, colour.z);
			}
			else
			{
				gfx.fill(10, 255, 10); // if point has no explicit colour, use a default
			}

			gfx.pushMatrix();
			gfx.translate(vox.x, vox.y, vox.z);
			gfx.box(vox.getNodeSize());
			gfx.popMatrix();
		}
	}
}