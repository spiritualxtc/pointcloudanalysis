/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.voxels;

import java.util.Random;

import toxi.geom.Vec3D;

/**
 *	Implementation of Cell which adds RGB color information 
 */
public class CellXYZRGB extends Cell
{

	public float r, g, b; //red,green,blue

	/**
	 * Standard constructor uses default xyz coord and generates a random color.
	 */
	public CellXYZRGB()
	{
		super();
		createRandomColour();
	}

	/**
	 * Create Cell with specified xyz coord. Generates a random color.
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 */
	public CellXYZRGB(float x, float y, float z)
	{
		super(x, y, z);
		createRandomColour();
	}

	/**
	 * Create Cell with specified xyz coord and rgb color.
	 * @param x coord
	 * @param y coord
	 * @param z coord
	 * @param r red color between 0-1f
	 * @param g green color between 0-1f
	 * @param b blue color between 0-1f
	 */
	public CellXYZRGB(float x, float y, float z, float r, float g, float b)
	{
		super(x, y, z);
		setColour(r, g, b);
	}

	/**
	 * Generate a random colour value
	 */
	private void createRandomColour()
	{
		Random rand = new Random();
		r = rand.nextFloat();
		g = rand.nextFloat();
		b = rand.nextFloat();
	}

	@Override
	public String toString()
	{
		return "<CellRGB> pos: " + super.toString() + " RGB: <" + r + "," + g + "," + b + ">";
	}

	@Override
	public void setColour(float r, float g, float b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
	}

	@Override
	public Vec3D getColour()
	{
		return new Vec3D(r, g, b);
	}
}
