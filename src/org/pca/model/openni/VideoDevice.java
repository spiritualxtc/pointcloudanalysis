/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.openni;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.openni.Device;
import org.openni.DeviceInfo;
import org.openni.ImageRegistrationMode;
import org.openni.SensorType;

/**
 * Controls Access to the sensor device via OpenNI
 * @author Spirit
 * @deprecated
 */
public class VideoDevice 
{
	private Device _device = null;
	private Map<SensorType, VideoStreamController> _controllers;
	
	/**
	 * Get handle to the OpenNI Device Object
	 * @return
	 */
	public Device getDevice() {return _device;}
	
	
	/**
	 * Gets the Color Controller
	 * @return
	 */
	public VideoStreamController getColorController() {return _controllers.get(SensorType.COLOR);}
	
	/**
	 * Gets the Depth Controller
	 * @return
	 */
	public VideoStreamController getDepthController() {return _controllers.get(SensorType.DEPTH);}
	
	/**
	 * Gets the Controller by it's SensorType
	 * @param sensor
	 * @return
	 */
	public VideoStreamController getController(SensorType sensor) {return _controllers.get(sensor);}
	
	

	
	/**
	 * Create Instance of a VideoDevice.
	 */
	public VideoDevice()
	{
		// Create Hashmap for the controllers
		_controllers = new HashMap<>();
	}

	
	/**
	 * Opens the SensorType for streaming
	 * @param devInfo The device to Open
	 * @param sensors Which sensors to create
	 */
	public synchronized void open(DeviceInfo devInfo, SensorType... sensors)
	{
		System.out.println("DEVICE: Opening");
		
		// Open the device
		_device = Device.open(devInfo.getUri());		
		_device.setDepthColorSyncEnabled(true);
	
	
		System.out.println("DEVICE: Open");
		System.out.println("DEVICE: " + _device.getDeviceInfo().getName());
		
		
		System.out.println("DEVICE: Create Streams");
		
		// Create Sensors
		for (SensorType sensor : sensors)
		{
			// Does device have the capabilities for this sensor?
			if (_device.hasSensor(sensor))
	        {
				// Create the Stream Controller
				VideoStreamController vsc = new VideoStreamController(_device, sensor);
				
				// Add to the Map
	        	_controllers.put(sensor, vsc);
	        	
	        	System.out.println("DEVICE: " + sensor + " Stream Created");
	        }
			else
			{
				System.out.println("DEVICE: " + sensor + " unavailable");
			}
		}
	}
	
	/**
	 * Starts the Stream Controllers
	 */
	public synchronized void start()
	{	
		// Set Image registration mode
		if (_device.isImageRegistrationModeSupported(ImageRegistrationMode.DEPTH_TO_COLOR))
		{
			System.out.println("DEVICE: Depth to Color Image Registration is Supported");
			_device.setImageRegistrationMode(ImageRegistrationMode.DEPTH_TO_COLOR);
			System.out.println("DEVICE: Set");
		}
		else
		{
			System.out.println("DEVICE: Settings Image Registration Mode is not Supported");		
		}		
		System.out.println("DEVICE: Start VideoStreams");
		
		
		// Start Streaming
		Collection<VideoStreamController> con = _controllers.values();
		for (VideoStreamController vsc : con)
			vsc.start();
	}
	
	/**
	 * Stops the Stream Controllers
	 */
	public synchronized void stop()
	{
		System.out.println("DEVICE: Stop VideoStreams");
		
		Collection<VideoStreamController> con = _controllers.values();
		for (VideoStreamController vsc : con)
			vsc.stop();	
	}
	

	/**
	 * Disposes of the object / native handles
	 */
	public synchronized void dispose()
	{
		System.out.println("DEVICE: Disposing VideoStreams");
		
		// Dispose of the Streams
		Collection<VideoStreamController> con = _controllers.values();
		for (VideoStreamController vsc : con)
			vsc.dispose();	
		
		// Clear Controller Map
		_controllers.clear();
		
		// Close the Device
		System.out.println("DEVICE: Closing");
		
		if (_device != null)
			_device.close();
		
		System.out.println("DEVICE: Closed");
		
		_device = null;
	}
}
