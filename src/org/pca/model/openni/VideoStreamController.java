/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.openni;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.openni.Device;
import org.openni.SensorType;
import org.openni.VideoFrameRef;
import org.openni.VideoMode;
import org.openni.VideoStream;
import org.pca.model.adapter.IVideoAdapter;
import org.pca.model.utils.FPS;

/**
 * Controller for a video stream (Not to be confused with an MVC controller....)
 * Rename Me :)
 * @author Spirit
 * @deprecated
 */
public class VideoStreamController implements VideoStream.NewFrameListener 
{
	private VideoStream		_stream = null;
	private VideoFrameRef	_frame	= null;
	private Object			_lock	= null;

	private FPS		_fps			= null;
	private int		IMG_GRAB_FPS	= 15;
	private float	_timer			= 0.0f;
	private float	_delay			= 1.0f / IMG_GRAB_FPS;
	private int		_frameCounter	= 0;

	
	
	/**
	 * Gets the number of frames that have been processed
	 * @return
	 */
	public int getFrameCounter() {return _frameCounter;}

	
	/**
	 * 
	 * @param device
	 * @param sensorType
	 */
	public VideoStreamController(Device device, SensorType sensorType)
	{
		_stream = VideoStream.create(device, sensorType);

		
		
		System.out.println(_stream.getSensorType() + ": Create Stream Controller");
		
		VideoMode mode = _stream.getVideoMode();
		
       	System.out.println("\t- Resolution: " + mode.getResolutionX() + "x" + mode.getResolutionY());
    	System.out.println("\t- FPS: " + mode.getFps());
		System.out.println("\t- Pixel Format = " + _stream.getVideoMode().getPixelFormat());
		
		
		_lock = new Object();
		
		_fps = new FPS();
	}


	/**
	 * Start the Stream
	 */
	public synchronized void start()
	{
		System.out.println(_stream.getSensorType() + ": Start Stream Controller");
		
		// Set listener
		_stream.addNewFrameListener(this);	
		
		// Start Stream
		_stream.start();
	}
	
	
	/**
	 * Stop the Stream
	 */
	public synchronized void stop()
	{	
		if (_stream == null)
			return;
		
		System.out.println(_stream.getSensorType() + ": Stopping sensor");
		
		synchronized(_lock)
		{
			System.out.println(_stream.getSensorType() + ": Stop Stream Controller");

			// Remove Listener
			_stream.removeNewFrameListener(this);
			
			// Stop Streaming
			_stream.stop();
		}
		
		System.out.println(_stream.getSensorType() + ": Stopped");
	}
	
	
	/**
	 * Disposes of the object
	 */
	public void dispose()
	{
		if (_stream == null)
			return;
		
		SensorType sensor = _stream.getSensorType();
		
		//Stop Streaming
		stop();
		
		synchronized(_lock)
		{
			// Release Previous Frame
	    	if (_frame != null)
	    	{
	    		System.out.println(sensor + ": Destroying Video Stream Reference");
	    		
	    		_frame.release();
	    		_frame = null;
	    	}
						
			System.out.println(sensor + ": Destroying Video Stream ");
			
			// Destroy the Stream
			_stream.destroy();
			_stream = null;
		}
		
		
		System.out.println(sensor + ": Disposed!");
	}
	
	
	/**
	 * Adapts data from the last stored video frame to a custom format :)
	 * 
	 * @param adapter
	 */
	public synchronized <T> T getData(IVideoAdapter<T> adapter)
	{
		if (_frame == null || adapter == null)
			return null;
		
		// Validate adapter
		if (adapter.getFormat() != _frame.getVideoMode().getPixelFormat())
			throw new IllegalArgumentException();
		
		
		T result = null;
		
		// You can wait your bloody turn :)
		synchronized(_lock)
		{
			// Get the ByteBuffer
			ByteBuffer buffer = _frame.getData().order(ByteOrder.LITTLE_ENDIAN);
		
			// Rewind the buffer position
			buffer.rewind();
			
			// Adapt data
			result = adapter.adapt(_frame.getWidth(), _frame.getHeight(), buffer);
			
			// Rewind the Buffer, as it appears to be shared
		//	buffer.rewind();			
		}
		
		return result;
	}
	
	
	/**
	 * Event listener called by OpenNI when a frame is ready
	 * @param stream reference to the video stream
	 */
    @Override
    public synchronized void onFrameReady(VideoStream stream) 
    {
    	synchronized(_lock)
    	{
	    	_fps.update();
	 
	    	// TEMPORARY
	    	// Only process frame if a certain amount of time has passed.
	    	_timer += _fps.getFrameTime();
	    	if (_timer < _delay)
	    	{
	    		return;
	    	}
	    	_timer -= _delay;
	    	
	    	// Increment Frame Counter
	    	++_frameCounter;
	    	
	    	// Release Previous Frame
	    	if (_frame != null)
	    	{
	    		_frame.release();
	    	}
	    	
	    	// Obtain New Frame
    		_frame = stream.readFrame();
    	}
    }


	/**
	 * Gets a reference to the VideoStream object.
	 * 
	 * DO NOT USE THIS METHOD FOR ACCESSING IMAGES FROM STREAM.
	 * 
	 * It's purely here to facilitate CoordinateConverter.convertDepthToWorld() elsewhere in the
	 * code which requires a reference to VideoStream. 
	 * FIXME Refactor the framework so that this can be done without giving access to this object.
	 * 
	 * @return this controllers VideoStream reference, which might be null.
	 */
	public VideoStream getVideoStream()
	{
		return _stream;
	}


    /*
     * The following commented out functions 
     * were from the original event based handler.
     * They haven't been removed as the event 
     * based handler may come in handy at a later date
     * for other reasons. 
     * Such as using an observer / notify pattern.
     */
    
//	/**
//	 * Adds a viewer
//	 * @param viewer
//	 * @return
//	 */
//	public boolean addViewer(IVideoStreamViewer viewer)
//	{
//		// Don't allow nulll listeners
//		if (viewer == null)
//			throw new IllegalArgumentException();
//		
//		// Check if added already
//		if (_viewers.contains(viewer))
//			return false;
//		
//		// Add Consumer
//		_viewers.add(viewer);
//		
//		return true;
//	}
	
	
//	/**
//	 * Removes the Viewer
//	 * @param viewer
//	 * @return
//	 */
//	public boolean removeViewer(IVideoStreamViewer viewer)
//	{
//		if (viewer == null)
//			throw new IllegalArgumentException();
//		
//		return _viewers.remove(viewer);
//	}
}
