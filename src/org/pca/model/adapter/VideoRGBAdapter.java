/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.adapter;

import java.nio.ByteBuffer;

import org.openni.PixelFormat;

import processing.core.PImage;

/**
 * Converts the RGB Channels of ByteBuffer to PImage
 * @author Spirit
 *
 */
public class VideoRGBAdapter implements IVideoPImageAdapter 
{
	private PImage _image = null;
	
	@Override
	public PixelFormat getFormat() {return PixelFormat.RGB888;}
	
	@Override
	public PImage adapt(int width, int height, ByteBuffer buffer)
	{
		// Create the image
		if (_image == null || _image.width != width || _image.height != height)
		{
			System.out.println("Image:" + width + "x" + height);
			_image = new PImage(width, height, PImage.RGB);
		}
	
		// Copy Image
		int pos = 0;
		while (buffer.remaining() > 0)
		{
            int red = (int)buffer.get() & 0xFF;
            int green = (int)buffer.get() & 0xFF;
            int blue = (int)buffer.get() & 0xFF;
			
            _image.pixels[pos++] = (0xFF << 24) | (red << 16) | (green << 8) | blue;
		}
		
		// Update the image : Some Processing thing
		_image.updatePixels();
	
		return _image;
	}

}
