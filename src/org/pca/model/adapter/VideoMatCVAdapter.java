/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.adapter;

import java.nio.ByteBuffer;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.openni.PixelFormat;

/**
 * Converts ByteBuffer to an OpenCV Mat [Image Matrix] format used by OpenCV
 * @author Spirit
 *
 */
public class VideoMatCVAdapter implements IVideoAdapter<Mat>
{
	final int CHANNEL_COUNT = 3;
	
	private byte[] _channelData = null;
	private Mat _image = null;
	
	/**
	 * Converts only RGB
	 */
	@Override
	public PixelFormat getFormat() {return PixelFormat.RGB888;}
	
	/**
	 * 
	 */
	@Override
	public Mat adapt(int width, int height, ByteBuffer buffer)
	{
		// Create the Matrix Buffer
		if (_channelData == null || (_channelData.length != width * height * CHANNEL_COUNT))
		{
			_channelData = new byte[width * height * CHANNEL_COUNT];
		}
		  
		// Create the Image Matrix
		if (_image == null || _image.cols() != width || _image.rows() != height)
		{
			_image = new Mat(height, width, CvType.CV_8UC3);
		}	
		
		
		// Copy : probs could be optimized.
		// #javaisslow
		int pos = 0;
      
		while (buffer.remaining() > 0) 
		{
			_channelData[pos+2] = buffer.get();
			_channelData[pos+1] = buffer.get();
			_channelData[pos+0] = buffer.get();
   
			pos+=3;
		} 
		
      	// Copy Image Data
		_image.put(0, 0, _channelData);		

		return _image;
	}		
}
