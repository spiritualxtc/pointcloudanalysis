/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.adapter;

import java.nio.ByteBuffer;

import org.openni.PixelFormat;

import processing.core.PImage;

/**
 * Converts a Depth Frame Bytebuffer to a PImage
 * @author Spirit
 *
 */
public class VideoDepthAdapter implements IVideoPImageAdapter 
{
	private static final float MAX_DEPTH = 8000.0f; // experimentally found 
	private PImage _image = null;
	
	@Override
	public PixelFormat getFormat() {return PixelFormat.DEPTH_1_MM;}
	
	@Override
	public PImage adapt(int width, int height, ByteBuffer buffer)
	{
		// Create the image
		if (_image == null || _image.width != width || _image.height != height)
		{
			System.out.println("Image:" + width + "x" + height);
			_image = new PImage(width, height, PImage.RGB);
		}
	
		// Copy Image
		int pos = 0;
		buffer.rewind();
	    while(buffer.remaining() > 0) 
        {
	    	// Pull a short representing depth (in mm units) from the buffer
	    	// then cast the short to an int
			int depthInMillimeters = (int)buffer.getShort() & 0xFFFF;
			
			// Normalize depth within the maximum depth
			float normalisedDepth = Math.max(0.0f,
					Math.min(1.0f, (float) depthInMillimeters / MAX_DEPTH));

			// Map the normalized depth between 0-255
			short pixel = (short) (normalisedDepth * 255);
			
			// Create new pixel in AARRGGBB format where RGB are the depth value
			_image.pixels[pos] = 0xFF000000 | (pixel << 16) | (pixel << 8) | (pixel << 0);
			pos++;
        }
		
		// Update the image : Some Processing thing
		_image.updatePixels();
	
		return _image;
	}
}
