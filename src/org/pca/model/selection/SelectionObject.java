/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.selection;

import processing.core.PVector;
import toxi.geom.AABB;
import toxi.geom.Vec3D;

/**
 * Information regarding a single object in the scene at a point in time
 * @author Spirit
 *
 */
public class SelectionObject 
{
	private AABB _aabb;
	
	public AABB getAABB() {return _aabb;}
	
	/**
	 * 
	 */
	public SelectionObject()
	{
		// Use negative bounds, so the first insertion compresses the box to a single point.
		_aabb = new AABB(new Vec3D(), new Vec3D(-10000, -10000, -10000));
	}
	
	
	/**
	 * Adds a processing point
	 * @param coord3d
	 */
	public void addPoint(PVector coord3d)
	{
		addPoint(new Vec3D(coord3d.x, coord3d.y, coord3d.z));
	}
	
	
	/**
	 * Adds a Toxiclib point
	 * @param coord3D
	 */
	public void addPoint(Vec3D coord3D)
	{
		// Update Bound Box
		_aabb.includePoint(coord3D);
		
		// Update Voxels
		
		// Update Points
	}

}
