/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.selection;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.pca.model.IDevice;

import processing.core.PVector;
import toxi.geom.AABB;

/**
 * Selects Objects
 * @author Spirit
 *
 */
public class Selection 
{
	private Collection<AABB> _boxes; 

	private IDevice _device = null;
	
//	private FloatBuffer _pointBuffer = null;

	
	/**
	 * Get the buffer
	 * @return
	 */
//	public FloatBuffer getPointBuffer() {return _pointBuffer;}

	
	
	
	/**
	 * Returns the boxes
	 * @return
	 */
	public Collection<AABB> getBoxes() 
	{
		return Collections.unmodifiableCollection(_boxes);
	}
	
	
	
	/**
	 * 
	 */
	public Selection(IDevice device)
	{
		if (device == null) {
			throw new IllegalArgumentException();
		}
	
		_device = device;
		
		_boxes = new ArrayList<AABB>();
	}
	
	
	/**
	 * Selects 3d points based on 2d image mask
	 * 
	 * 	ForEach Point in the cloud.
	 * 		//  Project to 2D Space
	 * 		//  Is the Target Pixel Black or White
	 * 		//  If White, what region does it belong too?
	 * 		//	Fix up 3D bound box
	 */
	public void processCloud(FloatBuffer positionCloud, byte[] channelData, MatOfRect rectangles)
	{
		// Create Map
		HashMap<Integer, SelectionObject> boxes = new HashMap<>();
		
		// Build Selection Groups
		for (Rect r : rectangles.toArray()) {
			boxes.put(r.hashCode(), new SelectionObject());
		}
		
		// Loop through every point
		while (positionCloud.remaining() > 0)
		{
			// Get Position
    		PVector vector = new PVector();
    		vector.x = positionCloud.get();
    		vector.y = positionCloud.get();
    		vector.z = positionCloud.get();
    	
    		// Skip a number of points to speed up processing
    		int numSkips = 3 * 10; 
    		positionCloud.position(Math.min(positionCloud.position() + numSkips, positionCloud.capacity())); // check min to not go out of bounds
    		
    		// Clip Zero-Depth points
    		if (vector.z == 0) {
    			continue;
    		}
    		
    		// Map 3D Camera Point to a 2D point in the Image   		
    		PVector pos = _device.MapCameraPointToColorSpace(vector);
    		
    		// Selection Region
    		Rect r = determineSelection(pos, channelData, rectangles);
    		if (r == null) {
    			continue;
    		}
    		
    		// Get AABB
    		SelectionObject obj = boxes.get(r.hashCode());
    		if (obj == null) {
    			continue;
    		}
    		
    		// Add to Bound Box OR adjust position of BoundBox
    		obj.addPoint(vector);
		}
		
		// Rewind Buffer
		positionCloud.rewind();
		
		// Update Boxes
		_boxes.clear();
		for (SelectionObject obj : boxes.values()) {
			_boxes.add(obj.getAABB());
		}
	}
	
		
	/**
	 * Determine the rectangle that is in the selection
	 * @param pixelCoord
	 * @param channelData
	 * @param rectangles
	 * @return
	 */
	private Rect determineSelection(PVector pixelCoord, byte[] channelData, MatOfRect regions)
	{
		// Ideally: Advanced Flexible Method (Not Implemented)
		// Using a basic procedural style due to time constraints
		// but shares the same logic flow
		/*
		 *  This should be possible using:
		 *  	- Multiple Transformation Processes:
		 *  		Transforms the 3D point, by some process.
		 * 		- Each different transformed result is passed into multiple selection tests
		 * 		- An Identification process is used for grouping selections together and identifying which group the point belongs too
		 * 		- A Post Process to clean up the result.
		 * 
		 * EG:
		 * 	- Transform 1: Pass through. Doesn't modify the value
		 *    - Selection Test 1: Depth Clipping. 
		 *    	Ignores the point if z > 4.0 	(Any point more than 4 meters from the camera)
		 *    - Selection Test 2: Depth Clipping. 
		 *    	Ignores the point if z < 0.5	(Any point less than 0.5 meters from the camera)
		 *    ...
		 *  - Transform 2: 2D Projection. Projects the 3D point into a 2D image plane 
		 *    - Selection Test 1: Hit Test. 
		 *    	Binary Image Test. Selection Passes, only if the pixel is white.
		 *    ...
		 *  - Identification Process:
		 *    - ID Process: Bound Box Intersection 
		 *    	Determine which rectangle the 2D point belongs too.
		 *  - Post Process:
		 *    - Process 1: Voxel Culling
		 *    	Build an object only voxel grid, removing any points that do not have enough points to reflect the object
		 */
		
		
		// Round to the nearest pixel
		int x = Math.round(pixelCoord.x);
		int y = Math.round(pixelCoord.y);
		
		// Image Hit Test
		if (isImageHit(channelData, x, y) == false) {
			return null;
		}
		
		// Determine the Rectangle that is hit
		return determineRect(regions, x, y);
	}
	
	
	
	/**
	 * Determine whether the pixel at (x, y) is white.
	 * @param _channelData Array of RGB data
	 * @param x position along X-Axis 
	 * @param y position along Y-Axis
	 * @return Whether the pixel is white
	 */
	private boolean isImageHit(byte[] channelData, int x, int y)
	{
		// Bounds Check
		if (x < 0 || y < 0 || x >= _device.getColorWidth() || y >= _device.getColorHeight()) {
			return false;
		}
		
		// Calculate Index
		int index = (x + y * _device.getColorWidth()) * 3;
		int b = Byte.toUnsignedInt(channelData[index++]);
		int g = Byte.toUnsignedInt(channelData[index++]);
		int r = Byte.toUnsignedInt(channelData[index++]);
		
		// Hit White Pixel ?
		return (r == 255 && g == 255 && b == 255);
	}
	
	
	/**
	 * Determine the rectangle that the point belongs too
	 * @param regions
	 * @param x
	 * @param y
	 * @return
	 */
	private Rect determineRect(MatOfRect regions, int x, int y)
	{
		// Iterate through rectangles.
		for (Rect r : regions.toArray())
		{
			if (r.contains(new Point(x, y))) {
				return r;
			}
		}
		return null;
	}
}
