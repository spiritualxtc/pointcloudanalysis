/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.utils;


public class Lerp
{
	/**
	 * 
	 * @param source
	 * @param sourceMin
	 * @param sourceMax
	 * @param destMin
	 * @param destMax
	 * @return
	 */
	static public float doIt(float source, float sourceMin, float sourceMax, float destMin, float destMax)
	{
		float percent = (source-sourceMin) / (sourceMax - sourceMin);
		float mapped = (percent * (destMax - destMin)) + destMin;
		return mapped;
	}
}
