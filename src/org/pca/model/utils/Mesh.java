/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.utils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collection;
import java.util.HashMap;

import processing.core.PGraphics;
import processing.opengl.PGL;

/**
 * General purpose Mesh class for storing information on the GPU for rendering to OpenGL (via Processing API)
 * 
 * TODO: Add support for multiple primitive types
 * Not really needed with regards to this application however
 * more for general purpose class
 * @author Spirit
 *
 */
public class Mesh 
{
	/**
	 * Stream Information
	 * @author Spirit
	 *
	 */
	public class MeshData
	{
		int vbo;
		int elements;
		
		public MeshData(int vbo, int elements)
		{
			this.vbo = vbo;
			this.elements = elements;
		}
	}
	
	private HashMap<String, MeshData> _streams;
	private PGraphics _graphics;
	private int _primitiveType = PGL.POINTS;
	private int _vertices = 0;
	
	/**
	 * Create the Mesh
	 * @param g Graphics Object
	 */
	public Mesh(PGraphics g)
	{
		_graphics = g;
		_streams = new HashMap<>();
	}
	
	
	/**
	 * Adds a data Stream to the mesh
	 * @param attrib Name of the stream. Matches the Attribute in the GLSL shader
	 * @param elements Number of elements used to represent a set of data.
	 * Position (XYZ) would be 3 elements
	 * Color (RGBA) would be 4 elements
	 *  
	 */
	public void addStream(String attrib, int elements)
	{
		// Allocate Buffer
		IntBuffer buffer = IntBuffer.allocate(1);
		
		// Begin GL
		PGL gl = _graphics.beginPGL();
		
		// Generate a Buffer
		gl.genBuffers(1, buffer);
		
		// Add Int to Array
		_streams.put(attrib, new MeshData(buffer.get(0), elements));
		
		// End GL
		_graphics.endPGL();
		
		System.out.println("MESH: Create Stream " + attrib + "(" + _streams.get(attrib).vbo + ")");
	}
	
	
	
	/**
	 * Sets the Data to the vertex buffer, using the whole buffer
	 * @param attrib Name of the Stream
	 * @param buffer Buffer containing the data
	 */
	public void setStream(String attrib, FloatBuffer buffer)
	{
		assert attrib != null && buffer != null;

		if (buffer != null) {
			setStream(attrib, buffer, buffer.capacity());	
		}
		
	}
	
	
	/**
	 * Sets the Data to the vertex buffer, using only a portion of the buffer
	 * @param stream Name of the stream
	 * @param buffer Buffer containing the data
	 * @param capacity Section of the buffer to use.
	 */
	public void setStream(String attrib, FloatBuffer buffer, int capacity)
	{
		if (buffer == null)
			return;
		
		// Get Stream
		MeshData data = _streams.get(attrib);
		
		if (data == null)
		{
			System.out.println("No Stream");
			return;
		}
		
		// Validate Capacity
		if (capacity % data.elements != 0)
			throw new IllegalArgumentException();
		
		// Begin GL
		PGL gl = _graphics.beginPGL();
		
		// Bind Vertex Data, Send to GPU. 3 floats. XYZ
		gl.bindBuffer(PGL.ARRAY_BUFFER, data.vbo);
	    gl.bufferData(PGL.ARRAY_BUFFER, Float.BYTES * capacity, buffer, PGL.DYNAMIC_DRAW);
	    
	    // Rewind Buffer
	    buffer.rewind();
	    
	    // End GL
	    _graphics.endPGL();
	    
	    // Get Vertex Count. Minimize Vertex Count
	    int verts = capacity / data.elements;
	    
	    if (verts < _vertices || _vertices == 0)
	    	_vertices = verts;
	}
	
	/**
	 * Draws the Mesh
	 * @param shaderProgram OpenGL Shader ID
	 */
	public void draw(int shaderProgram)
	{
		PGL gl = _graphics.beginPGL();
		
		// Get Keys
		Collection<String> keys = _streams.keySet();
		
		// Enable Vertex Attributes, and Bind Vertex Data
		for (String key : keys)
		{
			// Get Data
			MeshData data = _streams.get(key);
			
			// Get Location of Atribute in Shader
			int location = gl.getAttribLocation(shaderProgram, key);
			gl.enableVertexAttribArray(location);
			
			gl.bindBuffer(PGL.ARRAY_BUFFER, data.vbo);
			
			// Setup Attrib Pointer
			gl.vertexAttribPointer(location, data.elements, PGL.FLOAT, false, Float.BYTES * data.elements, 0);
		}
		
		// Draw Arrays
		gl.drawArrays(_primitiveType, 0, _vertices);
		
		// Disable Vertex Attributes
		for (String key : keys)
		{
			// Get Location of Atribute in Shader
			int location = gl.getAttribLocation(shaderProgram, key);
			gl.disableVertexAttribArray(location);
		}
		
		// Remove Binding
		gl.bindBuffer(PGL.ARRAY_BUFFER, 0);
		
		_graphics.endPGL();
	}

}
