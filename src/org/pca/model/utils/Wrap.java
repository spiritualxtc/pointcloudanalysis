/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.utils;


public class Wrap
{
	
	/**
	 * Shift the given val by the shift amount. If the resulting value goes beyond min or max, it will be wrapped around
	 * to be within [min,max] (inclusive).
	 * 
	 * @param val
	 *            is the value to be shifted, must be within min and max (inclusive).
	 * @param shift
	 *            the amount to shift the int by.
	 * @param min
	 *            low bound number (inclusive).
	 * @param max
	 *            upper bound number (inclusive). Must be >= min.
	 * @return the wrapped value
	 */
	static public int wrap(int val, int shift, int min, int max)
	{
		if (val > max || val < min)
		{
			throw new IllegalArgumentException("val must be within min and max inclusive");
		}
		if (max < min)
		{
			throw new IllegalArgumentException("Max must be >= min");
		}

		int diff = max - min + 1;
		int result = val + shift;

		if (result > max)
		{
			return result - diff;
		}
		else if (result < min)
		{
			return result + diff;
		}
		else
		{
			return result;
		}

	}
}
