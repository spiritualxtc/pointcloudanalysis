/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.utils;


/**
 * Simple FPS Counter
 * @author Spirit
 *
 */
public class FPS
{
	public interface OnSecondListener
	{
		public void second(long fps);
	}
	
	private long _frameRate = 0;
	private long _frameCount = 0;
	
	private double _frameTime = 0.0f;
	private double _timer = 0.0f;
	
	private long _updateLast = 0;
	
	public long getFrameRate()  { return _frameRate;}
	public float getFrameTime() { return (float) _frameTime; }
	
	
	
	private OnSecondListener _listener = null;
	
	
	/**
	 * 
	 */
	public FPS()
	{
		start();
	}
	
	/**
	 * Start the timer
	 */
	public void start()
	{
		_timer = 0.0;
		_updateLast = System.currentTimeMillis();
	}
	
	
	/**
	 * Update the timer
	 */
	public void update()
	{
		// Calculate Time since last update.
		long time = System.currentTimeMillis();
		
		_frameTime = (time - _updateLast) / 1000.0;
		_updateLast = time;
		
		_timer += _frameTime;
		_frameCount++;
		
		// Update Timer
		if (_timer > 1.0)
		{
			// Assign and Reset
			_frameRate = _frameCount;
			_frameCount = 0;
			
			_timer -= 1.0;
			
			if (_listener != null)
				_listener.second(_frameRate);
		}
	}
}
