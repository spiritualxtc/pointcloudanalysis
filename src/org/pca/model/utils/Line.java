/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.utils;


/**
 * Represents a line
 * @author Spirit
 */
public class Line 
{
	public float x1;
	public float y1;
	public float x2;
	public float y2;
	
	/**
	 * Creates a line
	 * @param fx1
	 * @param fy1
	 * @param fx2
	 * @param fy2
	 */
	public Line(float fx1, float fy1, float fx2, float fy2)
	{
		x1 = fx1;
		y1 = fy1;
		x2 = fx2;
		y2 = fy2;
	}
}
