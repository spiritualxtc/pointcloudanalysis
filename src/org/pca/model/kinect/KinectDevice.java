/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model.kinect;

import java.nio.FloatBuffer;

import org.pca.model.IDevice;

import KinectPV2.KinectPV2;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

/*
 * Wraps the KinectPV2 around an interface
 */
public class KinectDevice implements IDevice
{
	private KinectPV2 _kinect;
//	private PApplet _applet;
	
	
	/**
	 * 
	 */
	public KinectDevice()
	{

	}
	
	@Override
	public int getColorWidth() {return KinectPV2.WIDTHColor;}
	
	@Override
	public int getColorHeight() {return KinectPV2.HEIGHTColor;}
	
	@Override
	public int getDepthWidth() {return KinectPV2.WIDTHDepth;}

	@Override
	public int getDepthHeight() {return KinectPV2.HEIGHTDepth;}
	
	@Override
	public FloatBuffer getColorChannelData()
	{
		return _kinect.getColorChannelBuffer();
	}
	
	@Override
	public FloatBuffer getPointCloudColorPosition()
	{
		return _kinect.getPointCloudColorPos();
	}
	
	@Override
	public FloatBuffer getPointCloudDepthPosition()
	{
		return _kinect.getPointCloudDepthPos();
	}
	
	@Override
	public PVector MapCameraPointToColorSpace(PVector camera)
	{
		return _kinect.MapCameraPointToColorSpace(camera);
	}
	
	
	/**
	 * Is the Kinect Open
	 */
	@Override
	public boolean isOpen() {return _kinect != null;}
	
	
	/**
	 * Initialise the Kinect Device
	 */
	public boolean initialise(PApplet applet)
	{
//		_applet = applet;
		_kinect = new KinectPV2(applet);
		
		_kinect.enableDepthImg(true);
		_kinect.enableColorImg(true);
		_kinect.enableInfraredImg(true);
		_kinect.enableColorPointCloud(true);
		_kinect.enablePointCloud(true);
		_kinect.setLowThresholdPC(0);
		_kinect.setHighThresholdPC(65000);
		
		_kinect.enableCoordinateMapperRGBDepth(true);
		
		_kinect.init();
		
		System.out.println("Kinect PV2 is Initialised");
		System.out.println("Color Resolution: " + getColorWidth() + "x" + getColorHeight());
		System.out.println("Depth Resolution: " + getDepthWidth() + "x" + getDepthHeight());
		
		return true;
	}

	/**
	 * Close the Device
	 */
	public void close()
	{
		_kinect.closeDevice();
	}
	
	/**
	 * Releases KinectPV2 API
	 */
	public void dispose()
	{
		_kinect.dispose();
	}
	
	


	@Override
	public int[] getRawColorData()
	{
		if (_kinect == null) {
			return null;
		}
			
		// FIXME TODO HACK HACK HACK: This function call prevents a weird bloody bug that makes no bloody sense.
		// The images all come out black without this call.
		@SuppressWarnings("unused")
		PImage image = _kinect.getColorImage();
		
		// Get Raw Data
		int[] pixels = _kinect.getRawColor();
		
		return pixels;
	}
	
	
}
