/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.opencv.core.Core;
import org.pca.model.kinect.KinectDevice;

import processing.core.PApplet;

/**
 * Controls access to the model
 * @author Spirit
 *
 */
public class PCAFacade 
{
	/**
	 * Event handler for modules : Might be useful for the Viewer
	 * @author Spirit
	 *
	 */
	public interface OnModuleChanged
	{
		public void onModuleChanged(Collection<IModule> modules);
	}
	
	
	static PCAFacade _instance = null; 	
	
	/**
	 * Create an instance of the Model.
	 * Initialises all the Libraries needed
	 * @return
	 */
	static public PCAFacade initialise()
	{
		if (_instance != null) {
			throw new ExceptionInInitializerError();
		}
			
		// Load OpenNI Library
		// The OpenNI
//		System.out.print("OpenNI2 - Library ...");
//		System.loadLibrary("openni2");
//		System.out.println("Loaded");

		// Load OpenCV Library
		System.out.print("Open CV - Library ...");
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		System.out.println("loaded");

		// Initialise OpenNI
//		System.out.print("OpenNI2 ...");
//		OpenNI.initialize();
//		System.out.println("Initialised");		
		
		// Create the Instance
		_instance = new PCAFacade();
		
		return _instance;		
	}
	
	/**
	 * Destroys OpenNI
	 * @return
	 */
	static public void shutdown()
	{
//		OpenNI.shutdown();
	}
	
	
	private KinectDevice _kinect = null;
	private ArrayList<IModule> _modules;
	private OnModuleChanged _moduleChanged = null;
	
	
	/**
	 * Get Collection of Modules
	 * @return
	 */
	public Collection<IModule> getModules() {return Collections.unmodifiableCollection(_modules);}
	
	
	/**
	 * Get Reference to the device
	 * @return
	 */
	public IDevice getDevice() {return _kinect;}
	
	
	/**
	 * Creates the Model.
	 * This is created indirectly.
	 * Use PCAFacade.initialise();
	 */
	private PCAFacade()
	{
		// Create Wrapper Class
		_kinect = new KinectDevice();
		
		// Create Module List
		_modules = new ArrayList<IModule>();
	}
	
	/**
	 * Adds a module
	 * @param module Module to add
	 * @return Whether the module was successfully added.
	 */
	public boolean addModule(IModule module)
	{
		if (module == null) {
			throw new IllegalArgumentException();
		}
		
		boolean result = _modules.add(module); 
		
		// Fire Event
		if (result && _moduleChanged != null) 
		{
			_moduleChanged.onModuleChanged(getModules());
		}
		
		return result;
	}
	
	/**
	 * Removes a Module
	 * @param module Module to remove
	 * @return Whether the module was successfully removed
	 */
	public boolean removeModule(IModule module)
	{
		if (module == null) {
			throw new IllegalArgumentException();
		}
		
		// Stop Module 
		module.stop();
		
		boolean result = _modules.remove(module);
		
		// Fire Event
		if (result && _moduleChanged != null) 
		{
			_moduleChanged.onModuleChanged(getModules());
		}
		
		return result; 
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public IModule getModule(String id)
	{
		IModule module = null;
		
		// Loop through all modules
		for (IModule m : _modules)
		{
			if (m.getName().equals(id))
			{
				module = m;
				break;
			}
		}
		
		return module;
	}
	
	
	
	/**
	 * Initialise the Kinect
	 * @param applet
	 * @return
	 */
	public boolean initialiseKinect(PApplet applet)
	{
		return _kinect.initialise(applet);
	}
	
	
	
	
	
	
	/**
	 * Essentially the same as calling OpenNI.enumerateDevices();
	 * @return A list of devices that OpenNI recognises
	 */
//	public List<DeviceInfo> enumerateDevices()
//	{
//		return OpenNI.enumerateDevices();
//	}
	
	/**
	 * Start the device
	 */
	public void start()
	{
		// Stop all the Modules
		for (IModule module : _modules)
			module.start();	
	}
	
	
	/**
	 * Stop the Modules
	 */
	public void stop()
	{	
		// Stop all the Modules
		for (IModule module : _modules)
			module.stop();
	}
	

	/**
	 * Cleanup
	 */
	public void dispose()
	{
		// Stop Modules
		stop();
		
		// Cleanup the Modules
		_modules.clear();
		
		// Cleanup
		if (_kinect != null)
			_kinect.dispose();
		_kinect = null;
	}
	
	
}
