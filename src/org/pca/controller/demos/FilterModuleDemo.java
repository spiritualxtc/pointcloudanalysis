/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.controller.demos;

import org.pca.controller.App;
import org.pca.controller.modules.FilterDemoModule;

public class FilterModuleDemo extends App
{
	public static void main(String args[])
	{
		new FilterModuleDemo();
	}

	@Override
	protected void addModules()
	{
		_model.addModule(new FilterDemoModule(_model.getDevice()));
	}

}
