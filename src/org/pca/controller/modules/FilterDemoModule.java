/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.controller.modules;

import org.pca.model.IDevice;
import org.pca.model.Module;
import org.pca.model.filters.AdaptiveThresholdFilter;
import org.pca.model.filters.BlurFilter;
import org.pca.model.filters.CannyEdgeFilter;
import org.pca.model.filters.ContourFilter;
import org.pca.model.filters.GrayscaleFilter;
import org.pca.model.filters.HueFilter;
import org.pca.model.filters.MatBGRRootFilter;
import org.pca.model.filters.MatChannelDataFilter;
import org.pca.model.filters.RecognitionFilter;
import org.pca.model.filters.ThresholdFilter;
import org.pca.view.layer.camera.Camera;
import org.pca.view.layer.camera.CameraBuilder;
import org.pca.view.layer.drawable.ContourDrawable;
import org.pca.view.layer.drawable.MatOfRectDrawable;
import org.pca.view.layer.drawable.MatToPImageFilter;
import org.pca.view.layer.layers.DrawableLayer;
import org.pca.view.utils.Transform;


/**
 * Top level detection class for accessing object detection and/or recognition in a frame
 * Currently just a lot of experiments
 * @author Spirit
 *
 */
public class FilterDemoModule extends Module
{
	// Input / tree-root
	private MatBGRRootFilter	_colorFilter = null;
	
	// Outputs / tree-leaves
	
	/**
	 * Creates the Detection Module
	 * @param device
	 */
	public FilterDemoModule(IDevice device) 
	{
		super(device, "detection", "Detection", 1000/30);
		
		// Create Root Filters
		_colorFilter = new MatBGRRootFilter(device.getColorWidth(), device.getColorHeight());

		// Create Filters
		RecognitionFilter recog = new RecognitionFilter("res/lbpcascade_cokecan_v3.1.xml");		// Object Recognition "Coke Can"
		GrayscaleFilter grayscale = new GrayscaleFilter();
		BlurFilter blur = new BlurFilter(3.0);
		ThresholdFilter binary = new ThresholdFilter(127);
		AdaptiveThresholdFilter threshold = new AdaptiveThresholdFilter();
		CannyEdgeFilter canny = new CannyEdgeFilter();
		ContourFilter contour = new ContourFilter();
		HueFilter hue = new HueFilter();
		
		MatChannelDataFilter channels = new MatChannelDataFilter();
		
		// Tree 1 - Recognition & Color Extraction via Hue (HSV Color Format)
		_colorFilter.addFilter(recog);		
		_colorFilter.addFilter(hue);
	
		hue.addFilter(channels);
		
		// Tree 2 - Grayscal, Blurring, CannyEdge & Contour Mapping [incomplete]
		// Requires an improved graphing algorithm over the Contour ouptut to be usable.
		_colorFilter.addFilter(grayscale);
		grayscale.addFilter(blur);
		blur.addFilter(threshold);
		blur.addFilter(canny);
		canny.addFilter(contour);
		
		grayscale.addFilter(binary);
		
		// Tree 3 - Experiment for testing contour outputs.
		//grayscale.addFilter(canny);
		//canny.addFilter(blur);
		//threshold.addFilter(contour);
		
		
		
		// Setup Output Layers
		// Color
		MatToPImageFilter colorOut = new MatToPImageFilter();
		_colorFilter.addFilter(colorOut);
		
		// Grayscale
		MatToPImageFilter grayOut = new MatToPImageFilter();
		grayscale.addFilter(grayOut);
		
		// Blur
		MatToPImageFilter blurOut = new MatToPImageFilter();
		blur.addFilter(blurOut);
		
		// Binary
		MatToPImageFilter binaryOut = new MatToPImageFilter();
		binary.addFilter(binaryOut);
		
		// Threshold
		MatToPImageFilter threshOut = new MatToPImageFilter();
		threshold.addFilter(threshOut);
		
		// Canny
		MatToPImageFilter cannyOut = new MatToPImageFilter();
		canny.addFilter(cannyOut);
		
		// Hue
		MatToPImageFilter hueOut = new MatToPImageFilter();
		hue.addFilter(hueOut);
		
		// Contour Drawable
		ContourDrawable contourOut = new ContourDrawable(1920, 1080);
		contour.addFilter(contourOut);
		
		
		
		
		// Setup Layers
		
		// Create 2D Camera 
		Camera camera = new CameraBuilder().create();

		// Create Transformations. Positions the layers.
		Transform colorTransform = new Transform();
		colorTransform.scale(0.33f);
		
		Transform grayTransform = new Transform();
		grayTransform.scale(0.33f);
		grayTransform.translate(0.33f, 0.0f, 0.0f);
		
		Transform blurTransform = new Transform();
		blurTransform.scale(0.33f);
		blurTransform.translate(0.66f, 0.0f, 0.0f);
		
		Transform threshTransform = new Transform();
		threshTransform.scale(0.33f);
		threshTransform.translate(0,  0.33f, 0);
		
		Transform binaryTransform = new Transform();
		binaryTransform.scale(0.33f);
		binaryTransform.translate(0.33f,  0.33f, 0);
		
		Transform cannyTransform = new Transform();
		cannyTransform.scale(0.33f);
		cannyTransform.translate(0.66f, 0.33f, 0.0f);
		
		Transform hueTransform = new Transform();
		hueTransform.scale(0.33f);
		hueTransform.translate(0.33f, 0.66f, 0.0f);
		
		// Add Layers
		addLayer(new DrawableLayer(camera, "Color Image", colorOut, colorTransform));
		addLayer(new DrawableLayer(camera, "Object Recognition (2D)", new MatOfRectDrawable(1920, 1080, recog.getDetections()), colorTransform));
		
		addLayer(new DrawableLayer(camera, "Grayscale Image", grayOut, grayTransform));
		addLayer(new DrawableLayer(camera, "Blur Image", blurOut, blurTransform));
		addLayer(new DrawableLayer(camera, "Threshold Image", threshOut, threshTransform));
		addLayer(new DrawableLayer(camera, "Binary Image", binaryOut, binaryTransform));
		
		addLayer(new DrawableLayer(camera, "Canny Edge Image", cannyOut, cannyTransform));

		addLayer(new DrawableLayer(camera, "Contour Map", contourOut, cannyTransform));
		addLayer(new DrawableLayer(camera, "Hue Mask Image", hueOut, hueTransform));
}	
	
	
	
	/**
	 * Process the Frame
	 */
	@Override
	public synchronized void onProcessFrame(float timeDelta)
	{	
		// Apply Filters
		if (getDevice().isOpen() == false)
			return;
		
		int[] data = getDevice().getRawColorData();
		
		if (data != null)
		{
			_colorFilter.filter(data);
		}
	}
}
