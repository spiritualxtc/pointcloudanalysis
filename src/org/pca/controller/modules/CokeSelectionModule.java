/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.controller.modules;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.pca.model.IDevice;
import org.pca.model.Module;
import org.pca.model.filters.HueFilter;
import org.pca.model.filters.MatBGRRootFilter;
import org.pca.model.filters.MatChannelDataFilter;
import org.pca.model.filters.RecognitionFilter;
import org.pca.model.selection.Selection;
import org.pca.model.utils.Mesh;
import org.pca.model.voxels.CellXYZRGB;
import org.pca.model.voxels.VoxelOctree;
import org.pca.model.voxels.VoxelOctreeXYZRGB;
import org.pca.view.layer.camera.Camera;
import org.pca.view.layer.camera.CameraBuilder;
import org.pca.view.layer.drawable.MatOfRectDrawable;
import org.pca.view.layer.drawable.MatToPImageFilter;
import org.pca.view.layer.layers.BoundBoxLayer;
import org.pca.view.layer.layers.CameraMouseLayer;
import org.pca.view.layer.layers.DrawableLayer;
import org.pca.view.layer.layers.MeshLayer;
import org.pca.view.layer.layers.VoxelOctreeLayer;
import org.pca.view.layer.layers.VoxelSelectionLayer;
import org.pca.view.utils.Transform;

import processing.core.PApplet;
import toxi.geom.AABB;
import toxi.geom.Vec3D;

/**
 * Demo module wires up several components to solve the problem of analysing input from a sensor, detecting a coke can,
 * building a persistent voxel representation of the scene and selecting the voxels associated with the can.
 */
public class CokeSelectionModule extends Module 
{
	private static final String	SHADER_ATTRIB_VERTEX	= "vertex";
	private static final String	SHADER_ATTRIB_COLOR		= "color";

	// Filters which process the image streams
	private MatBGRRootFilter		_colorFilter	= null;
	private RecognitionFilter		_outputRegions	= null;
	private MatChannelDataFilter	_outputImage	= new MatChannelDataFilter();

	// Data for rendering point cloud
	private FloatBuffer	_vertexBuffer	= null;
	private FloatBuffer	_colorBuffer	= null;

	// Point boxes
	private Selection		_selection		= null;
	private ArrayList<AABB>	_selectionCopy	= new ArrayList<>();
	
	// Voxels
	private VoxelOctreeXYZRGB		_voxels				= new VoxelOctreeXYZRGB(new Vec3D(-1,-1,0), 2);  // start in hard coded demo favourable location.
	private List<VoxelOctreeXYZRGB>	_voxelSelections	= new ArrayList<>();

	/**
	 * Constructor initialises the demo module
	 * @param device sensor input to capture data from
	 */
	public CokeSelectionModule(IDevice device) 
	{
		super(device, "detection", "Detection", 1000/30);
		
		// Create Selection 
		_selection = new Selection(device);
		
		// Configure voxeloctree
		float mm = 5;
		_voxels.setMinNodeSize(mm/1000f); 
		

		// Create Filters
		_colorFilter = new MatBGRRootFilter(device.getColorWidth(), device.getColorHeight());
		RecognitionFilter 		recog = new RecognitionFilter("res/lbpcascade_cokecan_v3.1.xml");	// Object Recognition
		HueFilter 				hue = new HueFilter((int) HueFilter.RED, 0.15f); // 15% colour spectrum spread
		
		// Assign Output Filters
		_outputRegions = recog;
		
		// Wire up filters
		_colorFilter.addFilter(recog);		
		_colorFilter.addFilter(hue);
		hue.addFilter(_outputImage);
		
		
		/*
		 *  Create Viewport Cameras 
		 */
		Camera camera2D = new CameraBuilder().create();
		Camera camera3D = new CameraBuilder()
				.perspective((float)Math.toRadians(55.0f), 0.1f, 1000.0f)
				.lookAt(0, 0, -2, 0, 0, 0, 0, -1, 0)
				.create();
		
		
		
		/*
		 *  Setup Stream Output Layers
		 */
		// Color stream output
		Transform colorTransform = new Transform();
		colorTransform.scale(0.2f);
		
		MatToPImageFilter colorOut = new MatToPImageFilter();
		_colorFilter.addFilter(colorOut);
		
		// Hue filtered stream output
		Transform hueTransform = new Transform();
		hueTransform.scale(0.2f);
		hueTransform.translate(0, 0.2f, 0);
		
		MatToPImageFilter hueOut = new MatToPImageFilter();
		hue.addFilter(hueOut);


		
				
		// Add Layers for 2D rendering
		addLayer(new DrawableLayer(camera2D, "2D: Colour Stream", colorOut, colorTransform));
		addLayer(new DrawableLayer(camera2D, "2D: Hue Filter Stream", hueOut, hueTransform));
		addLayer(new DrawableLayer(camera2D, "2D: Detection Boxes", new MatOfRectDrawable(1920, 1080, recog.getDetections()), colorTransform));
		
		// Add 3d camera control to viewport
		addLayer(new CameraMouseLayer(camera3D, "Input: Camera Control"));
		
		// Create the rendering layer which displays the point cloud in viewport
		createPointCloudRenderLayer(camera3D);
		
		// Add layer which displays a bounding box representing the selected points
		createObjectSelectionAABBLayer(camera3D);
		
		// Add layer which renders the persistent voxel data
		addLayer(new VoxelOctreeLayer(camera3D, "3D: Persistent Voxels", _voxels, true, false));

		// Add layer which renders the voxel octree lattice framework
		addLayer(new VoxelOctreeLayer(camera3D, "3D: Voxel Structure", _voxels, false, true));
		
		// Add layer which renders voxels selected by bounding boxes
		createSelectedVoxelsRenderLayer(camera3D);
	}


	
	
	
	/**
	 * Process the Frame
	 * Contains a lot of benchmarking code :)
	 */
	@Override
	public synchronized void onProcessFrame(float timeDelta)
	{	
		if (getDevice().isOpen() == false) { return; }
		
		
		
		/*
		 *  Grab raw color data
		 */
		long getRawDataStart = System.currentTimeMillis();
		int[] data = getDevice().getRawColorData();
		if (data == null) { return; }
		float getRawDataDuration = (System.currentTimeMillis() - getRawDataStart)/1000f;
		
		long filterImgsStart = System.currentTimeMillis();
		_colorFilter.filter(data);
		float filterImgsDuration = (System.currentTimeMillis() - filterImgsStart)/1000f;
		
		
		
		/*
		 *  Get the Point Cloud. Make copy of it for rendering (to sidestep threading issue)
		 */
		long cpyDataBuffsStart = System.currentTimeMillis();
		FloatBuffer vertexBuffer = getDevice().getPointCloudColorPosition();
		FloatBuffer colorBuffer = getDevice().getColorChannelData();
		_vertexBuffer = vertexBuffer.duplicate();
		_colorBuffer = colorBuffer.duplicate();
		vertexBuffer.rewind();
		colorBuffer.rewind();
		float cpyDataBuffsDuration = (System.currentTimeMillis() - cpyDataBuffsStart)/1000f;
		
		
		
		/*
		 *  Insert point cloud data into voxel grid
		 */
		long insertPointsToVoxelStart = System.currentTimeMillis();
		_voxels.insertPoints(vertexBuffer, colorBuffer, 100);
		float insertPointsToVoxelDuration = (System.currentTimeMillis() - insertPointsToVoxelStart)/1000f;
		
		
		
		/*
		 *  Select 3d points based on 2d image mask
		 */
		long selectPointsStart = System.currentTimeMillis();
		_selection.processCloud(vertexBuffer, _outputImage.getChannelData(), _outputRegions.getDetections());
		syncSelectionBoxes(_selection.getBoxes());
		float selectPointsDuration = (System.currentTimeMillis() - selectPointsStart)/1000f;
			
		
		
		/*
		 *  Create new octree with copied cell data for each detected object
		 */
		long createVoxelStart = System.currentTimeMillis();
		createVoxelSelections();
		float createVoxelDuration = (System.currentTimeMillis() - createVoxelStart)/1000f;
		
		
		
		/*
		 *  Output Benchmark info
		 */
		System.out.println(	"GetRawData: " 			+ getRawDataDuration + "s " +
							"FilterImgs: " 			+ filterImgsDuration + "s " +
							"CopyDataBufs: " 		+ cpyDataBuffsDuration + "s " +
							"InsPointsToVox: " 		+ insertPointsToVoxelDuration + "s " +
							"SelectPoints: " 		+ selectPointsDuration + "s " +
							"CreateVoxSelections: " + createVoxelDuration + "s");
				
	}





	/**
	 * Create new octree with copied cell data for each detected object
	 */
	private void createVoxelSelections()
	{
		_voxelSelections.clear();
		for (AABB aabb : _selectionCopy)
		{
			VoxelOctreeXYZRGB voxelSelection = _voxels.copyVoxels(aabb);
			if (voxelSelection != null)
			{
				_voxelSelections.add(voxelSelection);		
			}
		}
	}
	

	
	/**
	 * Syncs the local list of boxes
	 * @param boxes
	 */
	private void syncSelectionBoxes(Collection<AABB> boxes)
	{
		synchronized (this)
		{
			_selectionCopy.clear();
		
			for (AABB aabb : boxes)
			{				
				_selectionCopy.add(aabb);
			}
		}
	}
	
	

	
	/**
	 * Adds a layer
	 * @param camera3D which controls the layer
	 */
	private void createSelectedVoxelsRenderLayer(Camera camera3D)
	{
		addLayer( new VoxelSelectionLayer(camera3D, "3D: Selected Voxels", true, false, new VoxelSelectionLayer.VoxelSelectionLayerHandler()
		{	
			@Override
			public void onUpdate(PApplet applet, List<VoxelOctree<CellXYZRGB>> o) 
			{
				synchronized(this)
				{
					o.clear();
					
					for (VoxelOctree<CellXYZRGB> vox : _voxelSelections)
						o.add(vox);
				}
			}
			
			@Override
			public void onSetup(PApplet applet, List<VoxelOctree<CellXYZRGB>> o) 
			{

			}
		}));
	}



	/**
	 * Adds a layer
	 * @param camera3D which controls the layer
	 */
	private void createPointCloudRenderLayer(Camera camera3D)
	{
		addLayer( new MeshLayer(camera3D, "3D: Point Cloud", null, new MeshLayer.MeshLayerHandler() 
		{
			@Override
			public void onSetup(PApplet applet, Mesh mesh) 
			{
				mesh.addStream(SHADER_ATTRIB_VERTEX, 3);
				mesh.addStream(SHADER_ATTRIB_COLOR, 3);
			}

			@Override
			public void onUpdate(PApplet applet, Mesh mesh) 
			{
				mesh.setStream(SHADER_ATTRIB_VERTEX, _vertexBuffer);
				mesh.setStream(SHADER_ATTRIB_COLOR, _colorBuffer);
			}
		}));
	}




	/**
	 * Adds a layer which renders bounding boxes around the object selection
	 * @param camera3D which controls the layer
	 */
	private void createObjectSelectionAABBLayer(Camera camera3D)
	{
		addLayer(new BoundBoxLayer(camera3D, "3D: Object Recognitions", new BoundBoxLayer.BoundBoxLayerHandler()
		{	
			@Override
			public void onUpdate(PApplet applet, ArrayList<AABB> o) 
			{
				synchronized(this)
				{
					o.clear();
					
					for (AABB aabb : _selectionCopy)
						o.add(aabb);
				}
			}
			
			@Override
			public void onSetup(PApplet applet, ArrayList<AABB> o) 
			{

			}
		}));
	}
	
	
	
}
