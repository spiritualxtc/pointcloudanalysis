/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.controller;

import java.util.Collection;
import org.pca.model.IModule;
import org.pca.model.PCAFacade;
import org.pca.view.ViewManager;

import processing.core.PApplet;

/**
 * Application Controller
 * 
 */
abstract public class App implements IController
{
	protected PCAFacade		_model;
	protected ViewManager	_view;

	/**
	 * Create the Application
	 */
	public App()
	{
		// Initialise the Library
		_model = PCAFacade.initialise();

		// Setup up sensor device
//		_model.openDevice();

		// Add all processing modules
		addModules();

		// Give the view access to the controller
		_view = new ViewManager(this);

		// Start the Modules and the device
		_model.start();
		
		// Kinect PV2 
		// ----------
		// Initialise Libraries
		// Create View : Create Processing Component
		// Create KinectPV2 : Requires Processing Component
		// Create Module[s] : Requires KinectPV2
	}

	abstract protected void addModules();

	/*
	 * Implementation of IAppAction
	 */
	@Override
	public void onAppletInitialise(PApplet applet)
	{
		System.out.println("View Initialised");
		_model.initialiseKinect(applet);
	}
	

	/**
	 * Called by the View when the Application Quits.
	 */
	@Override
	public void onQuit()
	{
		// Dispose of any native handles & resources
		System.out.println("Closing");

		// Dispose of the viewer (for Processing API)
		System.out.println("Disposing Viewer");
		_view.dispose();

		// Dispose of Library
		_model.dispose();
		PCAFacade.shutdown();

		// Terminate Process. Hopefully everything else cleans up automatically
		// :)
		System.exit(0);
	}

	/*
	 * Implementation of IAppAction : Passthrough to Facade
	 */

	@Override
	public IModule getModule(String id)
	{
		return _model.getModule(id);
	}

	@Override
	public Collection<IModule> getModules()
	{
		return _model.getModules();
	}
}
