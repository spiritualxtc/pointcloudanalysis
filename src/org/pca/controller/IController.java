/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.controller;

import java.util.Collection;

import org.pca.model.IModule;

import processing.core.PApplet;

/**
 * This is temporary until Kim's GUI is in place and used with teh rest of hte stuff/
 * Not sure if this will be used once integration is done with the proper GUI
 * @author Spirit
 *
 */
public interface IController 
{
	/**
	 * Callback when teh Application has quit
	 */
	void onQuit();
	
	/**
	 * Callback when the Processing Applet has been initialised
	 */
	void onAppletInitialise(PApplet applet);
	
	/**
	 * Search for a module
	 * @param id ID of the module
	 * @return the Module
	 */
	IModule getModule(String id);
	
	/**
	 * Gets the list of all modules
	 * @return The list of all modules
	 */
	Collection<IModule> getModules();
	
	
}
