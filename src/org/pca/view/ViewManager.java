/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JCheckBox;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import org.pca.controller.IController;
import org.pca.model.IModule;
import org.pca.view.component.AppFrame;
import org.pca.view.component.ProcessingComponent;
import org.pca.view.layer.Layer;

/**
 * Driver class for the GUI and also incorporates event handlers
 * 
 * @author Kim
 *
 */
public class ViewManager
{
	private IController			_ctrl;
	private AppFrame			_appFrame;
	private ProcessingComponent	_component;
	private Collection<IModule>	_modules;
	private Collection<Layer>	_layers;

    /**
     * 
     * @param ctrl
     */
    public ViewManager(IController ctrl)
    {
        _ctrl = ctrl;
        _modules = (_ctrl != null) ? _ctrl.getModules() : null;

        _layers = new ArrayList<Layer>();
        for (IModule module : _modules)
        {
            _layers.addAll(module.getLayers());
        }

        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                _appFrame = new AppFrame(_modules, _layers);
                
                // Event Callback
                // KinectPV2 depends on the Processing Component for initialisation
                _ctrl.onAppletInitialise(_appFrame.getProcessing());
                
                handleEvents();
            }
        });
    }

    /**
     * Temporary main method to check layout
     * 
     * @param args
     */
//    public static void main(String[] args)
//    {
//        new ViewManager(null);
//    }

    /**
     * Add listeners
     */
    public void handleEvents()
    {
        // Register closing event
        _appFrame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {

                if (_ctrl != null)
                    _ctrl.onQuit();
            }
        });

        // Add listener for toggling main buttons
        for (JToggleButton button : _appFrame.getToolBarButtons())
        {
            button.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    IModule module = getModule(button.getText());
                    for (Layer layer : module.getLayers())
                    {
                        JCheckBox checkBox = getCheckBox(layer.toString());
                        if (button.isSelected() && !checkBox.isSelected())
                        {
                            addLayer(layer);
                            checkBox.setSelected(true);
                        }
                        else if (!button.isSelected() && checkBox.isSelected())
                        {
                            removeLayer(layer);
                            getCheckBox(layer.toString()).setSelected(false);
                        }
                    }
                }
            });
            // Default to Selected
            button.setSelected(true);
        }

        for (JCheckBox checkBox : _appFrame.getLayerCheckBoxes())
        {
            checkBox.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e)
                {
                    Layer layer = getLayer(checkBox.getText());
                    if (checkBox.isSelected())
                    {
                        addLayer(layer);
                    }
                    else if (!checkBox.isSelected())
                    {
                        removeLayer(layer);
                    }
                }
            });
            
            // Default to Selected
            checkBox.setSelected(true);
        }
        
        // Enable all layers, by default
        for (Layer layer : _layers)
        	_appFrame.getProcessing().addLayer(layer);
    }

    /**
     * Adds a layer to the processing component
     * @param layer
     */
    public void addLayer(Layer layer)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                _appFrame.getProcessing().addLayer(layer);
            }
        });
    }

    /**
     * Removes a layer from the processing component
     * @param layer
     */
    public void removeLayer(Layer layer)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                _appFrame.getProcessing().removeLayer(layer);
            }
        });
    }

    /**
     * Searches for a checkbox
     * @param name
     * @return
     */
    public JCheckBox getCheckBox(String name)
    {
        for (JCheckBox checkBox : _appFrame.getLayerCheckBoxes())
        {
            if (name.equals(checkBox.getText()))
            {
                return checkBox;
            }
        }
        return null;
    }
    
    /**
     * Searches for a layer
     * @param name
     * @return
     */
    public Layer getLayer(String name)
    {
        for (Layer layer : _layers)
        {
            if (name.equals(layer.toString()))
            {
                return layer;
            }
        }
        return null;
    }

    /**
     * Searches for a Module
     * @param name
     * @return
     */
    public IModule getModule(String name)
    {
        for (IModule module : _modules)
        {
            if (name.equals(module.getName()))
            {
                return module;
            }
        }
        return null;
    }

    // Methods from Viewer class

    /**
     * Redraw
     */
    public void paint(Graphics g)
    {
        synchronized (this)
        {
            _appFrame.paint(g);
        }
    }

    /**
     * Cleanup
     */
    public void dispose()
    {
    	
        synchronized (this)
        {
            _component = _appFrame.getProcessing();

            if (_component == null)
                return;

            _appFrame.remove(_component);

            _component.stop();
            // _component.dispose();
            _component = null;
        }
    }

}
