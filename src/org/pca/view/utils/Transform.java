/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.utils;


import processing.core.PGraphics;
import toxi.geom.Vec3D;


/**
 * Transform for an Object in 2D or 3D space
 * @author Spirit
 *
 */
public class Transform 
{
	private Vec3D _translation;
	private Vec3D _rotation;
	private Vec3D _scale;
	
	/**
	 * Set Translation
	 * @param x Set X Position
	 * @param y Set Y Position
	 * @param z Set Z Position
	 */
	public void translate(float x, float y, float z)
	{
		_translation.x = x;
		_translation.y = y;
		_translation.z = z;
	}
	
	/**
	 * Set Translation
	 * @param translate Translation Position
	 */
	public void translate(Vec3D translate)
	{
		_translation.x = translate.x;
		_translation.y = translate.y;
		_translation.z = translate.z;
	}	
		
	/**
	 * Set Scale
	 * @param s Set Scale across all axes
	 */
	public void scale(float s)
	{
		_scale.x = s;
		_scale.y = s;
		_scale.z = s;
	}
	
	/**
	 * Set Scale
	 * @param s Set scale for individual axes
	 */
	public void scale(float sx, float sy, float sz)
	{
		_scale.x = sx;
		_scale.y = sy;
		_scale.z = sz;
	}
	
	/**
	 * Set Scale
	 * @param scale Set scale
	 */
	public void scale(Vec3D scale)
	{
		_scale.x = scale.x;
		_scale.y = scale.y;
		_scale.z = scale.z;
	}
	
	/**
	 * Set Pitch (X-Axis Rotation)
	 * @param pitchRad The angle in radians
	 */
	public void setPitch(float pitchRad) {_rotation.x = pitchRad;}
	
	/**
	 * Set Yaw (Y-Axis Rotation)
	 * @param yawRad The angle in radians
	 */
	public void setYaw(float yawRad) {_rotation.y = yawRad;}
	
	/**
	 * Set Roll (Z-Axis Rotation)
	 * @param rollRad The angle in radians
	 */
	public void setRoll(float rollRad) {_rotation.z = rollRad;}		
	
	/**
	 * Gets the Pitch
	 * @return The angle in Radians
	 */
	public float getPitch() {return _rotation.x;}
	
	/**
	 * Gets the Yaw
	 * @return The angle in Radians
	 */
	public float getYaw() {return _rotation.y;}
	
	/**
	 * Gets the Roll
	 * @return The angle in Radians
	 */
	public float getRoll() {return _rotation.z;}	
	
	
	/**
	 * Gets the X-Axis Translation Component
	 * @return X-Axis Translation
	 */
	public float getTranslationX() {return _translation.x;}
	
	/**
	 * Gets the Y-Axis Translation Component
	 * @return Y-Axis Translation
	 */
	public float getTranslationY() {return _translation.y;}
	
	/**
	 * Gets the Z-Axis Translation Component
	 * @return Z-Axis Translation
	 */
	public float getTranslationZ() {return _translation.z;}		
			
	/**
	 * Gets the X-Axis Scale Component
	 * @return X-Axis Scaling
	 */
	public float getScaleX() {return _scale.x;}
	
	/**
	 * Gets the Y-Axis Scale Component
	 * @return Y-Axis Scaling
	 */
	public float getScaleY() {return _scale.y;}
	
	/**
	 * Gets the Z-Axis Scale Component
	 * @return Z-Axis Scaling
	 */
	public float getScaleZ() {return _scale.z;}
	
	
	
	/**
	 * Create the Transformation
	 */
	public Transform()
	{
		_translation = new Vec3D();
		_rotation = new Vec3D();
		_scale = new Vec3D(1.0f, 1.0f, 1.0f);
	}
	
	
	/**
	 * Calls graphics.push()
	 * It's only to help with transform ordering
	 * @param g Graphics Object
	 */
	public void push(PGraphics g)
	{
		g.pushMatrix();
	}
	
	
	/**
	 * Calls graphics.pop()
	 * It's only to help with transform ordering
	 * @param g Graphics Object
	 */
	public void pop(PGraphics g)
	{
		g.popMatrix();
	}
	
	
	/**
	 * Sets up the transformation matrix for Processing API
	 * @param g Graphics Object
	 */
	public void apply(PGraphics g)
	{
		g.translate(_translation.x, _translation.y, _translation.z);
		g.rotate(_rotation.y, 0, 1, 0);
		g.rotate(_rotation.x, 1, 0, 0);
		g.rotate(_rotation.z, 0, 0, 1);
		g.scale(_scale.x, _scale.y, _scale.z);
	}
}

