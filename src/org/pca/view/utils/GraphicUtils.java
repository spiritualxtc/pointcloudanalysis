/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.utils;

import processing.core.PGraphics;

public class GraphicUtils 
{
	
	/**
	 * Draws an Axis
	 * @param length Length for the Axis
	 * @param g Graphics Object
	 */
	public static void drawAxis(float length, PGraphics g)
	{
		float weight = g.strokeWeight;
		
		g.strokeWeight = 1.0f;
	
		g.stroke(255,0,0);
		g.line(0,0,0,length,0,0);
		g.stroke(0,255,0);
		g.line(0,0,0,0,length,0);
		g.stroke(0,0,255);
		g.line(0,0,0,0,0,length);	
		
		g.strokeWeight = weight;
	}
	
	
	/**
	 * Draw a Grid
	 * @param size Size of the Axis
	 * @param density Density of the Axis
	 * @param g Graphics Object
	 */
	public static void drawGrid(float size, int density, PGraphics g)
	{
		float weight = g.strokeWeight;
		
		g.strokeWeight = 1.0f;
		
		g.stroke(192, 192, 192);
		
		for (float i = -size * 0.5f; i<=size * 0.5f; i += size/(float)density)
		{
			g.line(i, 0, -size * 0.5f, i, 0.0f, size * 0.5f);
		}
		
		for (float i = -size * 0.5f; i<=size * 0.5f; i += size/(float)density)
		{
			g.line(-size * 0.5f, 0, i, size * 0.5f, 0.0f, i);
		}
		
		g.strokeWeight = weight;
	}
	
	
	
	
	
	/**
	 * Hidden Constructor
	 */
	private GraphicUtils()
	{
	}
}
