/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer;

import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Drawable class for a generic layer
 * @author Spirit
 *
 */
public interface IDrawable 
{
	/**
	 * Width of the Drawable
	 * @return
	 */
	int getWidth();
	
	/**
	 * Height of the Drawable
	 * @return
	 */
	int getHeight();
	
	/**
	 * Sets up the drawble
	 * @param applet Applet object
	 */
	void setup(PApplet applet);
	
	/**
	 * Update the drawable
	 * @param frameTime How long the last frame took
	 * @param applet Applet object
	 */
	void update(float frameTime, PApplet applet);
	
	/**
	 * Draws the Object
	 * @param width Width of the Processing Display Window
	 * @param height Height of the Processing Display Window
	 * @param g Graphics object
	 */
	void draw(int width, int height, PGraphics g);
}
