/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.drawable;

import java.util.Collection;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.pca.model.filters.IFilter;

import processing.core.PGraphics;


/**
 * Draws the contours
 * @author Spirit
 * TODO:
 * https://en.wikipedia.org/wiki/Nearest-neighbor_chain_algorithm
 */
public class ContourDrawable extends SimpleDrawable implements IFilter<Collection<MatOfPoint>>
{
	private Collection<MatOfPoint> _points;
	
	/**
	 * Creates the Drawable
	 * @param width Width of the drawable
	 * @param height Height of the drawable
	 */
	public ContourDrawable(int width, int height) 
	{
		super(width, height);
	}


	@Override
	public void draw(int width, int height, PGraphics g) 
	{
		if (_points == null)
			return;
		
		g.fill = false;
		g.strokeWeight = 4;
		g.strokeColor = g.color(255,  0, 0);
		
		synchronized (this)
		{
			// Loop through the Objects
			for (MatOfPoint points : _points)
			{
				Point[] array = points.toArray();
				
				int red = points.isContinuous() == true ? 0 : 255;
				
				// Draw Contour for the Object
				for (int i=0; i<array.length - 1; ++i)
				{
					float w = i / (float)array.length;
					int green = (int)(w * 255.0f);
						
					g.strokeColor = g.color(red, green, 255 - green);
					
					Point p1 = array[i];
					Point p2 = array[i + 1];
					
					g.line((float)p1.x, (float)p1.y, (float)p2.x, (float)p2.y);
				}
			}
		}
	}
	
	@Override
	public void filter(Collection<MatOfPoint> input) 
	{
		synchronized (this)
		{
			_points = input;
		}
	}

}
