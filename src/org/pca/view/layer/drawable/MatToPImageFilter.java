/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.drawable;

import org.opencv.core.Mat;
import org.pca.model.filters.Filter;
import org.pca.view.layer.IDrawable;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

/**
 * Converts an OpenCV Image Matrix to a PImage for display purposes
 * @author Spirit
 *
 */
public class MatToPImageFilter extends Filter<Mat, PImage> implements IDrawable
{
	private PImage _image = null;
	private byte[] _channelData = null;

	@Override
	public int getWidth() 
	{
		return _image == null ? 1 : _image.width;
	}

	@Override
	public int getHeight() 
	{
		return _image == null ? 1 : _image.height;
	}
	
	
	@Override
	protected PImage doFilter(Mat input)
	{
		if (input == null) 
		{
			return null;
		}
		
		// Create the image
		if (_image == null || _image.width != input.cols() || _image.height != input.rows())
		{
			System.out.println("Image:" + input.cols() + "x" + input.rows());
			_image = new PImage(input.cols(), input.rows(), PImage.RGB);
		}
		
		// Create Channel Buffer
		if (_channelData == null || (_channelData.length != input.cols() * input.rows() * input.channels()))
		{
			_channelData = new byte[input.cols() * input.rows() * input.channels()];
		}
		
		// Get Data from Image matrix		
		input.get(0, 0, _channelData);
		
		// Copy to PImage
		int index = 0;
		for (int i=0; i<input.cols() * input.rows(); ++i)
		{
			int b = 0;
			int g = 0;
			int r = 0;
			
			if (input.channels() == 3)
			{
				// 3 Channels
				b = Byte.toUnsignedInt(_channelData[index++]);
				g = Byte.toUnsignedInt(_channelData[index++]);
				r = Byte.toUnsignedInt(_channelData[index++]);
			}
			else if (input.channels() == 1)
			{
				// A Single Channel
				int v = Byte.toUnsignedInt(_channelData[index++]);
				b = v;
				g = v;
				r = v;
			}
			
			// Convert Individual channel values to a combined RGB value
			int rgb = r << 16 | g << 8 | b << 0;
			
			// Update the Pixel Value - including alpha
			_image.pixels[i] = (0xFF << 24) | rgb;
		}
		
		// Update the Processing Image
		_image.updatePixels();
		

		return _image;
	}

	@Override
	public void setup(PApplet applet) 
	{
	
	}

	@Override
	public void update(float frameTime, PApplet applet) 
	{
	
	}

	@Override
	public void draw(int width, int height, PGraphics g) 
	{
		if (_image != null) {
			g.image(_image, 0, 0);
		}
	}
}
