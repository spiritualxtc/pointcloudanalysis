/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.drawable;

import org.pca.view.layer.IDrawable;

import processing.core.PApplet;

/**
 * Class for basic drawables that need knowledge about their dimensions
 * @author Spirit
 *
 */
public abstract class SimpleDrawable implements IDrawable 
{
	private int _width;
	private int _height;
	
	@Override
	public int getWidth() 
	{
		return _width;
	}

	@Override
	public int getHeight() 
	{
		return _height;
	}
	
	/**
	 * Create teh Drawable
	 * @param width Width of the Drawable
	 * @param height Height of the Drawable
	 */
	public SimpleDrawable(int width, int height)
	{
		_width = width;
		_height = height;
	}
	
	@Override
	public void setup(PApplet applet) {}
	
	@Override
	public void update(float frameTime, PApplet applet){}
}
