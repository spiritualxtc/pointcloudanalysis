/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.drawable;

import java.util.Collection;

import org.pca.model.filters.IFilter;
import org.pca.model.utils.Line;

import processing.core.PGraphics;

/**
 * Draws a collection of Lines [Used with the Hough Filter]
 * @author Spirit
 *
 */
public class LinesDrawableFilter extends SimpleDrawable implements IFilter<Collection<Line>>
{
	private Collection<Line> _lines;

	/**
	 * Creates the Drawable
	 * @param width Width of the Drawable
	 * @param height Height of the Drawable
	 */
	public LinesDrawableFilter(int width, int height)
	{
		super(width, height);
	}

	@Override
	public void draw(int width, int height, PGraphics g)
	{
		if (_lines == null)
			return;
		
		synchronized(this)
		{		
			g.strokeColor = g.color(0, 255, 0);
			for (Line line : _lines)
			{
				g.line(line.x1, line.y1, line.x2, line.y2);
			}
		}
	}

	@Override
	public void filter(Collection<Line> input)
	{
		synchronized(this)
		{
			_lines = input;
		}
	}
}
