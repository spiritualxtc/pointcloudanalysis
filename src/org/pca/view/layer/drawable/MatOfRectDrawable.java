/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.drawable;

import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;

import processing.core.PGraphics;

/**
 * Draws a number of Rectangles  
 * @author Spirit
 *
 */
public class MatOfRectDrawable extends SimpleDrawable 
{
	private MatOfRect _rects = null;
	

	
	/**
	 * Create the Drawable
	 * @param rects 
	 */
	public MatOfRectDrawable(int width, int height, MatOfRect rects)
	{
		super(width, height);
		
		_rects = rects;
		
		if (_rects == null)
			throw new IllegalArgumentException();
	}
	
	@Override
	public void draw(int width, int height, PGraphics g)
	{
		if (_rects == null)
			return;
		
		
		
		// Set Graphics Properties
		g.fill = false;
		g.strokeWeight = 4;
		g.strokeColor = g.color(255,  0, 0);
		
		// Iterate through rectangles
		for (Rect rect : _rects.toArray())
        {
        	int x = (int)(rect.x);
        	int y = (int)(rect.y);
        	int wth = (int)(rect.width);
        	int hgt = (int)(rect.height);
        	
        	g.rect(x, y, wth, hgt);
        }
		
	}

}
