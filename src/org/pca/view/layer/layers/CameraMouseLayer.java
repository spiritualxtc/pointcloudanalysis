/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import org.pca.view.layer.Layer;
import org.pca.view.layer.camera.Camera;
import org.pca.view.utils.GraphicUtils;
import org.pca.view.utils.Transform;

import processing.core.PApplet;
import processing.core.PGraphics;


/**
 * Layer specifically to deal with shared camera mouse movement
 * 
 * This class is as much a hack as it is a simplification
 * @author Spirit
 *
 * TODO:
 * Keyboard input is buggy.
 */
public class CameraMouseLayer extends Layer
{
	private int _lastX = 0;
	private int _lastY = 0;
	
	
	public CameraMouseLayer(Camera camera) 
	{
		this(camera, "Camera Control");
	}
	
	public CameraMouseLayer(Camera camera, String name) 
	{
		super(camera, name);
	}
	@Override
	public void setup(PApplet applet) 
	{
		
	}

	@Override
	protected void update(float frameTime, PApplet applet) 
	{
	    // Update Camera Rotation
		int dx = applet.mouseX - _lastX;
		int dy = applet.mouseY - _lastY;
		
		// Update Mouse Coordinate
		_lastX = applet.mouseX;
		_lastY = applet.mouseY;	
		
		
		if (applet.mouseButton == PApplet.LEFT)
		{
			float yaw = getCamera().getYaw();
			float pitch = getCamera().getPitch();
			yaw += (dx) * 0.005f;
			pitch += (dy) * 0.005f;
			
					
			
			getCamera().setYaw(yaw);
			getCamera().setPitch(pitch);
		}
		
			
		// Update Camera Zoom
		if (applet.mouseButton == PApplet.RIGHT)
		{
			float zoom = getCamera().getZoom();
			
			float distance = (float)Math.sqrt(dx * dx + dy * dy);
			
			zoom += distance * 0.025f * (float)Math.signum(dy);
			if (zoom < 0.01f)
				zoom = 0.01f;
			
			getCamera().setZoom(zoom);
		}
		
		if (applet.mouseButton == PApplet.CENTER)
		{
			// DY : Move in direction of camera....
			// DX : Pan Camera
			
			float yaw = getCamera().getYaw();
			float pitch = getCamera().getPitch();
			
			float dirx = 0.0f;
			float diry = 0.0f;
			float dirz = 0.0f;
			
			if (applet.keyCode != PApplet.SHIFT)
			{
				dirx = (float)Math.cos(yaw);
				diry = 0.0f;
				dirz = (float)Math.sin(yaw);
			}
			else
			{
				dirx = (float)Math.sin(yaw) * (float)Math.cos(pitch);
				diry = -(float)Math.sin(pitch);
				dirz = (float)Math.cos(yaw) * (float)Math.cos(pitch);
			}
			
			
			float tx = getCamera().getTranslationX();
			float ty = getCamera().getTranslationY();
			float tz = getCamera().getTranslationZ();
			
			float mul = 0.1f;
			
			tx += dirx * dy * mul;
			ty += diry * dy * mul;
			tz += dirz * dy * mul;
			
			getCamera().translate(tx, ty, tz);
		}
		
		
		if (applet.keyCode == 'R' || applet.keyCode == 'r')
		{
			getCamera().translate(0, 0, 0);
			getCamera().rotate(0, 0, 0);
		}
		
	
		
		

	}

	/**
	 * 
	 */
	@Override
	protected void draw(int width, int height, PGraphics g) 
	{
		Transform t = new Transform();
		
		float x = getCamera().getTranslationX();
		float y = getCamera().getTranslationY();
		float z = getCamera().getTranslationZ();
		t.translate(x, y, z);
		
		
		t.push(g);
		t.apply(g);
		g.strokeColor = g.color(0, 255, 0);
		//g.sphere(0.05f);
		GraphicUtils.drawAxis(0.1f, g);
		t.pop(g);
	}

}
