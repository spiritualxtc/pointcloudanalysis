/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;


import org.pca.view.layer.Layer;
import org.pca.view.layer.camera.CameraBuilder;

import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Debugging Layer
 * @author Spirit
 *
 */
public class DebugLayer extends Layer
{
	/**
	 * 
	 */
	public DebugLayer()
	{
		super(new CameraBuilder().create(), "Debug");
	}

	@Override
	public void setup(PApplet applet) 
	{
		
	}

	@Override
	public void update(float frameTime, PApplet applet) 
	{
		
	}

	@Override
	public void draw(int width, int height, PGraphics g)
	{
		g.textAlign = PApplet.LEFT;
		g.textAlignY = PApplet.TOP;
		g.text("FPS: " + g.parent.frameRate, 0, 0);
	}
	

}
