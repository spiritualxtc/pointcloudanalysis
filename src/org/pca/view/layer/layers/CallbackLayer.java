/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import org.pca.view.layer.Layer;
import org.pca.view.layer.camera.Camera;

import processing.core.PApplet;


/**
 * Layer that uses a callback function for updates
 * @author Spirit
 *
 * @param <T> Class used by the callback
 */
public abstract class CallbackLayer<T> extends Layer 
{
	public ILayerHandler<T> _handler; 
	

	/**
	 * Creates the callback Layer
	 * @param camera Camera used by the layer
	 * @param handler Callback Interface
	 */
	public CallbackLayer(Camera camera, ILayerHandler<T> handler)
	{
		this(camera, null, handler);
	}
	
	
	/**
	 * Creates the callback Layer
	 * @param camera Camera used by the layer
	 * @param name Name of the layer
	 * @param handler Callback Interface
	 */
	public CallbackLayer(Camera camera, String name, ILayerHandler<T> handler)
	{
		super(camera, name);
		
		_handler = handler;
	}
	
	
	/**
	 * Calls the Setup Callback method
	 * @param applet
	 * @param o
	 */
	protected void fireSetup(PApplet applet, T o)
	{
		if (_handler != null)
			_handler.onSetup(applet, o);
	}
	
	/**
	 * Calls the Update Clalback method
	 * @param applet
	 * @param o
	 */
	protected void fireUpdate(PApplet applet, T o)
	{
		if (_handler != null)
			_handler.onUpdate(applet, o);
	}
	
	

}
