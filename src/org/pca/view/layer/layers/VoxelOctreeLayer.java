/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import java.util.List;

import org.pca.model.voxels.CellXYZRGB;
import org.pca.model.voxels.VoxelOctree;
import org.pca.view.layer.Layer;
import org.pca.view.layer.camera.Camera;
import processing.core.PApplet;
import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * Renders a concrete VoxelOctreeGeneric of type CellXYZRGB
 */
public class VoxelOctreeLayer extends Layer
{
	private boolean							_drawOctree	= false;
	private boolean							_drawVoxels	= true;
	private VoxelOctree<CellXYZRGB>	_voxels		= null;

	/**
	 * Creates the OctTree
	 * @param camera Camera to use
	 * @param name Name of the layer
	 * @param voxels reference to the Voxel OctTree
	 * @param drawVoxels Draws the Leaf Octree Nodes
	 * @param drawOctree Draws the Framework of the Octree Node
	 */
	public VoxelOctreeLayer(Camera camera, String name, VoxelOctree<CellXYZRGB> voxels, boolean drawVoxels, boolean drawOctree)
	{
		super(camera, name);
		_voxels = voxels;
		_drawVoxels = drawVoxels;
		_drawOctree = drawOctree;
	}
	
	/**
	 * Creates the OctTree
	 * @param camera Camera to use
	 * @param voxels reference to the Voxel OctTree
	 * @param drawVoxels Draws the Leaf Octree Nodes
	 * @param drawOctree Draws the Framework of the Octree Node
	 */
	public VoxelOctreeLayer(Camera camera, VoxelOctree<CellXYZRGB> voxels, boolean drawVoxels, boolean drawOctree)
	{
		this(camera, "VoxelOctreeLayer", voxels,  drawVoxels, drawOctree);
	}

	@Override
	public void setup(PApplet applet)
	{

	}

	@Override
	protected void update(float frameTime, PApplet applet)
	{

	}

	@Override
	protected void draw(int width, int height, PGraphics g)
	{
		drawNode(_voxels, g);
	}

	/**
	 * Draws the Voxel OctTree Node
	 * @param vox Voxel Node
	 * @param g Graphics Objects
	 */
	private void drawNode(VoxelOctree<CellXYZRGB> vox, PGraphics g)
	{

		// Draw wire box for framework
		if (!vox.isLeafNode())
		{
			if (_drawOctree)
			{
				// Draw current node
				g.noFill();
				g.strokeWeight(1);
				g.stroke(50, 50, 255, 50); 

				g.pushMatrix();
				g.translate(vox.x, vox.y, vox.z);
				g.box(vox.getNodeSize());
				g.popMatrix();
			}

			// Draw Children
			VoxelOctree<CellXYZRGB>[] childNodes = vox.getChildren();
			for (int i = 0; i < 8; i++)
			{
				if (childNodes[i] != null) {
					drawNode(childNodes[i], g);
				}
			}
		}
		// Draw voxel
		else if (_drawVoxels && vox.getPoints() != null)
		{
			g.noStroke();

			// Colour the box
			List<CellXYZRGB> points = vox.getPoints();
			if (points.size() > 0)
			{
				Vec3D colour = points.get(0).getColour().scale(255); // using first point's colour
				g.fill(colour.x, colour.y, colour.z);
			} else
			{
				g.fill(10, 255, 10); // if point has no explicit colour, use a
										// default
			}

			g.pushMatrix();
			g.translate(vox.x, vox.y, vox.z);
			g.box(vox.getNodeSize());
			g.popMatrix();
		}
	}

	// class CellValPrint implements VoxelOctreeVisitorGeneric<CellXYZRGB>
	// {
	// @Override
	// public void visitNode(VoxelOctreeGeneric<CellXYZRGB> node)
	// {
	// if (node.isLeafNode())
	// {
	// System.out.println(node.toString());
	//
	// // Pull the cell info out and do something with it
	// List<CellXYZRGB> cells = node.getPoints();
	// for (CellXYZRGB c : cells)
	// {
	// System.out.println(c.toString());
	// }
	// }
	// }
	// }
}
