/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import java.util.ArrayList;
import java.util.List;

import org.pca.model.voxels.CellXYZRGB;
import org.pca.model.voxels.VoxelOctree;
import org.pca.view.layer.camera.Camera;

import processing.core.PApplet;
import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * Renders a concrete VoxelOctreeGeneric of type CellXYZRGB of a selected object
 */
public class VoxelSelectionLayer extends CallbackLayer<List<VoxelOctree<CellXYZRGB>>>
{
	public interface VoxelSelectionLayerHandler extends ILayerHandler<List<VoxelOctree<CellXYZRGB>>>
	{

	}

	private boolean									_drawOctree	= false;
	private boolean									_drawVoxels	= true;
	private List<VoxelOctree<CellXYZRGB>>	_octrees	= null;

	/**
	 * Creates the Layer
	 * @param camera Camera to use
	 * @param drawVoxels Draws the Leaf Octree Nodes
	 * @param drawOctree Draws the Framework of the Octree Node
	 * @param handler Callback Handler
	 */
	public VoxelSelectionLayer(Camera camera, boolean drawVoxels, boolean drawOctree,
			VoxelSelectionLayerHandler handler)
	{
		this(camera, "VoxelOctrees", drawVoxels, drawOctree, handler);
	}

	/**
	 * Creates the Layer
	 * @param camera Camera to use
	 * @param name Name of the layer
	 * @param drawVoxels Draws the Leaf Octree Nodes
	 * @param drawOctree Draws the Framework of the Octree Node
	 * @param handler Callback Handler
	 */
	public VoxelSelectionLayer(Camera camera, String name, boolean drawVoxels, boolean drawOctree,
			VoxelSelectionLayerHandler handler)
	{
		super(camera, name, handler);

		_octrees = new ArrayList<>();
		_drawOctree = drawOctree;
		_drawVoxels = drawVoxels;
	}

	@Override
	public void setup(PApplet applet)
	{
		fireSetup(applet, _octrees);
	}

	@Override
	protected void update(float frameTime, PApplet applet)
	{
		fireUpdate(applet, _octrees);
	}

	@Override
	protected void draw(int width, int height, PGraphics g)
	{
		for (VoxelOctree<CellXYZRGB> octree : _octrees)
		{
			drawNode(octree, g);
		}
	}

	/**
	 * Draws the Voxel OctTree Node
	 * @param vox Voxel Node
	 * @param g Graphics Objects
	 */
	private void drawNode(VoxelOctree<CellXYZRGB> vox, PGraphics g)
	{

		// Draw wire box for framework
		if (!vox.isLeafNode())
		{
			if (_drawOctree)
			{
				// Draw current node
				g.noFill();
				g.strokeWeight(1);
				g.stroke(50, 50, 255, 50);

				g.pushMatrix();
				g.translate(vox.x, vox.y, vox.z);
				g.box(vox.getNodeSize());
				g.popMatrix();
			}

			// Draw Children
			VoxelOctree<CellXYZRGB>[] childNodes = vox.getChildren();
			for (int i = 0; i < 8; i++)
			{
				if (childNodes[i] != null)
				{
					drawNode(childNodes[i], g);
				}
			}
		}
		// Draw voxel
		else if (_drawVoxels && vox.getPoints() != null)
		{
			g.noStroke();

			// Colour the box
			List<CellXYZRGB> points = vox.getPoints();
			if (points.size() > 0)
			{
				Vec3D colour = points.get(0).getColour().scale(255); // using first point's colour
				g.fill(colour.x, colour.y, colour.z);
			} else
			{
				g.fill(10, 255, 10); // if point has no explicit colour, use a default
			}

			g.pushMatrix();
			g.translate(vox.x, vox.y, vox.z);
			g.box(vox.getNodeSize());
			g.popMatrix();
		}
	}
}
