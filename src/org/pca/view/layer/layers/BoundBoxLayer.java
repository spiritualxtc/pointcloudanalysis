/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import java.util.ArrayList;

import org.pca.view.layer.camera.Camera;
import org.pca.view.utils.Transform;

import processing.core.PApplet;
import processing.core.PGraphics;
import toxi.geom.AABB;
import toxi.geom.Vec3D;

/**
 * Layer for displaying bounding boxes
 * @author spirit
 *
 */
public class BoundBoxLayer extends CallbackLayer<ArrayList<AABB>> 
{
	public interface BoundBoxLayerHandler extends ILayerHandler<ArrayList<AABB>>
	{
		
	}
	
	private ArrayList<AABB> _boxes = null;
	
	
	
	public BoundBoxLayer(Camera camera, BoundBoxLayerHandler handler)
	{
		this(camera, "AABBs", handler);
	}
	
	public BoundBoxLayer(Camera camera, String name, BoundBoxLayerHandler handler) 
	{
		super(camera, name, handler);
		
		// Create Boxes
		_boxes = new ArrayList<>();
	}
	

	/**
	 * 
	 */
	@Override
	public void setup(PApplet applet) 
	{
		_boxes.add(new AABB(new Vec3D(0, 0.5f, 0), new Vec3D(0.25f, 1.0f, 0.25f)));
		
		fireSetup(applet, _boxes);
	}
	

	/**
	 * 
	 */
	@Override
	protected void update(float frameTime, PApplet applet) 
	{
		fireUpdate(applet, _boxes);
	}

	
	/**
	 * 
	 */
	@Override
	protected void draw(int width, int height, PGraphics g) 
	{
		g.fill = false;
		g.strokeColor = g.color(0, 255, 0);
		
		// Draw Boxes
		for (AABB aabb : _boxes)
		{
			Transform t = new Transform();
			t.translate(aabb.x, aabb.y, aabb.z);
			
			t.push(g);
			t.apply(g);
			g.box(aabb.getExtent().x * 2.0f, aabb.getExtent().y * 2.0f, aabb.getExtent().z * 2.0f);
			t.pop(g);
		}
	}

}
