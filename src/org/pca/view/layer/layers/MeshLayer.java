/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import org.pca.model.utils.Mesh;
import org.pca.view.layer.camera.Camera;
import org.pca.view.utils.GraphicUtils;
import org.pca.view.utils.Transform;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.opengl.PShader;

/**
 * Displays a mesh
 * @author Spirit
 *
 */
public class MeshLayer extends CallbackLayer<Mesh>
{
	public interface MeshLayerHandler extends ILayerHandler<Mesh>
	{
		
	}


	
	private Mesh _mesh = null;
	private PShader _shader = null;
	private Transform _transform = null;
	
	private String _vsShader = "vert.glsl";
	private String _fsShader = "frag.glsl";
	
	
	public Mesh getMesh() {return _mesh;}
	
	
	
	/**
	 * 
	 * @param camera
	 * @param name
	 * @param transform
	 * @param handler
	 */
	public MeshLayer(Camera camera, Transform transform, MeshLayerHandler handler)
	{
		this(camera, null, transform, handler);
	}
	
	/**
	 * 
	 * @param camera
	 * @param name
	 * @param transform
	 * @param handler
	 */
	public MeshLayer(Camera camera, String name, Transform transform, MeshLayerHandler handler) 
	{
		this(camera, name, transform, null, null, handler);
	}	
	

	/**
	 * 
	 * @param camera
	 * @param name
	 * @param transform
	 * @param vsShader
	 * @param fsShader
	 * @param handler
	 */
	public MeshLayer(Camera camera, String name, Transform transform, String vsShader, String fsShader, MeshLayerHandler handler) 
	{
		super(camera, name, handler);
		
		_handler = handler;
		
		if (vsShader != null)
			_vsShader = vsShader;
		
		if (fsShader != null)
			_fsShader = fsShader;
		
		if (_transform != null)
			_transform = transform;
		else
			_transform = new Transform();
	}

	/**
	 * 
	 */
	@Override
	public void setup(PApplet applet) 
	{
		System.out.println("SETUP MESH");
		
		// Create The Mesh
		_mesh = new Mesh(applet.g);
		
		System.out.println("Loading Shader: " + _vsShader + ", " + _fsShader);
		
		// Load the Shader
		try
		{
			_shader = applet.loadShader(_fsShader, _vsShader);
		}
		catch (Exception e)
		{
			System.out.println("Failed to load shader: " + e.getMessage());
			e.printStackTrace();
		}
		// Fire Setup Event
		fireSetup(applet, _mesh);
	}

	@Override
	protected void update(float frameTime, PApplet applet) 
	{
		// Fire update Event
		fireUpdate(applet, _mesh);
	}

	@Override
	protected void draw(int width, int height, PGraphics g) 
	{
		if (_mesh == null || _shader == null)
		{
		//	System.out.println(_mesh + ", " + _shader);
			return;
		}
		
		// Debug Code
//		// Create The Mesh
//		if (_mesh == null)
//			_mesh = new Mesh(g);
//				
//		// Create the Shader
//		if (_shader == null)
//			_shader = g.loadShader(_fsShader, _vsShader);
		
		// Begin GL
		g.beginPGL();
		
		// Bind Shader
		_shader.bind();
		
		_transform.push(g);
		_transform.apply(g);
		
		// Draw Mesh
		_mesh.draw(_shader.glProgram);
				
		_transform.pop(g);
		
		// Unbind Shader
		_shader.unbind();
		
		GraphicUtils.drawAxis(1.0f, g);
//		GraphicUtils.drawGrid(50.0f, 100, g);	
		
		// End GL
		g.endPGL();
	}
	

}
