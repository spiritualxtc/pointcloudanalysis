/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.layers;

import org.pca.view.layer.IDrawable;
import org.pca.view.layer.Layer;
import org.pca.view.layer.camera.Camera;
import org.pca.view.utils.Transform;

import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Renders the Generic Drawable Object
 * @author Spirit
 *
 */
public class DrawableLayer extends Layer 
{
	private IDrawable _drawable;
	
	private Transform _transform = null;
	
	
	
	/**
	 * Sets the Transformation
	 * @param t Transformation Object
	 */
	public void setTransform(Transform t)
	{
		_transform = t;
		
		if (_transform == null)
			throw new IllegalArgumentException();
	}
	
	
	/**
	 * Create Layer
	 * @param camera Camera to Use
	 * @param drawable Drawable Object
	 */
	public DrawableLayer(Camera camera, IDrawable drawable) 
	{
		this(camera, null, drawable, new Transform());
	}
	
	/**
	 * 
	 * @param camera Camera to Use
	 * @param name Name of the layer
	 * @param drawable Drawable Object
	 */
	public DrawableLayer(Camera camera, String name, IDrawable drawable) 
	{
		this(camera, name, drawable, new Transform());
	}
	
	
	/**
	 * 
	 * @param camera Camera to Use
	 * @param drawable Drawable Object
	 * @param transform Transformation for the Drawable
	 */	
	public DrawableLayer(Camera camera, IDrawable drawable, Transform transform)
	{
		this(camera, null, drawable, transform);
	}
	
	
	/**
	 * 
	 * @param camera Camera to Use
	 * @param name Name of the layer
	 * @param drawable Drawable Object
	 * @param transform Transformation for the Drawable
	 */
	public DrawableLayer(Camera camera, String name, IDrawable drawable, Transform transform) 
	{
		super(camera, name);
	
		// Set teh Drawable
		_drawable = drawable;
		if (_drawable == null)
			throw new IllegalArgumentException();
		
		// Set teh Transform
		_transform = transform;
		if (_transform == null)
			throw new IllegalArgumentException();
	}
	
	
	
	

	@Override
	public void setup(PApplet applet)
	{
		_drawable.setup(applet);
	}

	@Override
	protected void update(float frameTime, PApplet applet) 
	{
		_drawable.update(frameTime, applet);
	}

	@Override
	protected void draw(int width, int height, PGraphics g) 
	{	
		// Reposition the layer in screen space
		// while preserving the drawables size ratio
		
		// Create Local Alignment Matrix
		Transform local = new Transform();
		
		// Create Global Identity Transformation
		if (_transform == null)
			_transform = new Transform();
		
		// Reposition
		float x = width * _transform.getTranslationX();
		float y = height * _transform.getTranslationY();
		
		local.translate(x, y, 0.0f);
		
		// Scale
		float srcWidth = _drawable.getWidth();
		float srcHeight = _drawable.getHeight();
		
		float scaleX = (float)width / srcWidth;
		float scaleY = (float)height / srcHeight;
		
		float sx = scaleX * _transform.getScaleX();
		float sy = scaleY * _transform.getScaleY();
		
		float scale = (sx < sy) ? sx : sy;
		local.scale(scale, scale, 1.0f);
		
		// Rotation
		local.setYaw(_transform.getYaw());
		
		// Set Transformation
		local.push(g);
		local.apply(g);
			
        // Draw the drawable
		_drawable.draw(width, height, g);
        
        // Remove Transformation
		local.pop(g);		
	}

}
