/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer;

import org.pca.view.layer.camera.Camera;

import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * Base Layer for drawing to the screen
 * @author Spirit
 *
 */
public abstract class Layer
{
	private Camera _camera;
	private String _name = null;
	
	/**
	 * Gets the Layers Camera
	 * @return
	 */
	public Camera getCamera() {return _camera;}
	
	
	/**
	 * Create the layer with no name
	 * @param camera Camera to use for the layer
	 */
	public Layer(Camera camera)
	{
		this(camera, null);
	}
	
	/**
	 * Creates the layer with a name
	 * @param camera Camera to use for the layer
	 * @param name "nice" Name of the layer
	 */
	public Layer(Camera camera, String name)
	{
		_camera = camera;
		_name = name;
	}

	/**
	 * Redraw the layer
	 * @param width
	 * @param height
	 * @param g
	 */
	final public void redraw(PApplet p)
	{
		// Update Frame
		update(1.0f / p.frameRate, p);
		
		// Setup Camera
		_camera.setupCamera(p.width, p.height, p.g);
		
		// Draw Frame
		draw(p.width, p.height, p.g);
	}

	/**
	 * Gets the layer name
	 * @return
	 */
	public String getName() { return _name; }
	
	/**
	 * Setup the Layer
	 * @param g
	 */
	public abstract void setup(PApplet applet); 
	
	/**
	 * Update the Layer
	 * @param frameTime
	 */
	protected abstract void update(float frameTime, PApplet applet);
	
	/**
	 * Draws the Layer
	 * @param width
	 * @param height
	 * @param g
	 */
	protected abstract void draw(int width, int height, PGraphics g);
	
	/**
	 * Default String output.
	 * Outputs the name, if it exists otherwise falls back to the generic toString() 
	 */
	@Override
	public String toString()
	{
		String name = getName();
		
		return name != null ? name : super.toString();
	}
}



