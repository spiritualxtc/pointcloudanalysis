/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;

import processing.core.PGraphics;

/**
 * Creates a 3D Perspective Matrix
 * @author Spirit
 *
 */
public class Perspective implements IProjection 
{
	
	private float _fov = (float)Math.PI / 2.0f;
	private float _near = 0.01f; // 1cm
	private float _far = 100.0f; // 100m
	

	/**
	 * Creates a 3D Perspective Matrix
	 * @param fov Field of View
	 * @param near Near Clipping Plane
	 * @param far Far Clipping Plane
	 */
	public Perspective(float fov, float near, float far)
	{
		_fov = fov;
		_near = near;
		_far = far;
	}

	@Override
	public void setProjectionMatrix(float width, float height, PGraphics g)
	{
		float aspect = width / height;
		
		g.perspective(_fov, aspect, _near, _far);
		
	}
	

}
