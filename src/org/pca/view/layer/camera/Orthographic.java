/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;

import processing.core.PGraphics;

/**
 * Orthographic 2.5D Projection Matrix
 * @author Spirit
 *
 */
public class Orthographic implements IProjection
{
	private float _left = 0.0f;
	private float _top = 0.0f;
	private float _right = 0.0f;
	private float _bottom = 0.0f;
	private float _near = 0.0f;
	private float _far = 0.0f;
	
	/**
	 * Creates an Orthogonal Projection Matrix
	 * @param left Left Extent of the Projection
	 * @param right Right Extent of the Projection
	 * @param bottom Bottom Extent of the Projection
	 * @param top Top of the Projection
	 */
	public Orthographic(float left, float right, float bottom, float top)
	{
		_left = left;
		_top = top;
		_right = right;
		_bottom = bottom;
		_near = 1.0f;
		_far = -1.0f;
	}
	
	@Override
	public void setProjectionMatrix(float width, float height, PGraphics g) 
	{
		g.ortho(_left, _right, _bottom, _top, _near, _far);
	}
}
