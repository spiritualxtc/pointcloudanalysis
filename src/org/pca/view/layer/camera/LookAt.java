/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;

import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * LookAt View Matrix
 * @author Spirit
 *
 */
public class LookAt implements IView 
{
	
	private Vec3D _eye;
	private Vec3D _target;
	private Vec3D _up;
	
	/**
	 * Creates a Default LookAt View Matrix
	 */
	public LookAt()
	{
		_eye = new Vec3D(0, 0, -1);
		_target = new Vec3D(0, 0, 0);
		_up = new Vec3D(0, 1, 0);
	}
	
	/**
	 * Creates a LookAt View Matrix
	 * @param eyeX Position of Camera
	 * @param eyeY Position of Camera
	 * @param eyeZ Position of Camera
	 * @param targetX Target Position of Camera
	 * @param targetY Target Position of Camera
	 * @param targetZ Target Position of Camera
	 * @param upX Up Direction of Camera
	 * @param upY Up Direction of Camera
	 * @param upZ Up Direction of Camera
	 * @return Reference to itself for object chaining
	 */
	public LookAt(float eyeX, float eyeY, float eyeZ, float targetX, float targetY, float targetZ, float upX, float upY, float upZ)
	{
		_eye = new Vec3D(eyeX, eyeY, eyeZ);
		_target = new Vec3D(targetX, targetY, targetZ);
		_up = new Vec3D(upX, upY, upZ);
	}
	
	@Override
	public void setViewMatrix(float width, float height, float zoom, PGraphics g) 
	{
		g.camera(_eye.x * zoom, _eye.y * zoom, _eye.z * zoom, _target.x, _target.y, _target.z, _up.x, _up.y, _up.z);	
	}
	

}
