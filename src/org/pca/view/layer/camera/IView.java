/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;

import processing.core.PGraphics;

/**
 * 
 * @author Spirit
 *
 */
public interface IView
{
	/**
	 * Sets the View Matrix
	 * @param width Width of the Display
	 * @param height Height of the Display
	 * @param g Graphics Object
	 */
	void setViewMatrix(float width, float height, float zoom, PGraphics g);
}
