/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;


import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * 
 * @author Spirit
 *
 */
public class Camera
{
	private IProjection _projection = null;
	private IView _view = null;
	
//	private Viewport _viewport = null;	// Might get moved to the layer object itself
	
	private Vec3D _translation;
	private Vec3D _scale;
	private Vec3D _rotation;
	
	private float _zoom = 1.0f;
	
	/**
	 * Set Translation
	 * @param x Set X Position
	 * @param y Set Y Position
	 * @param z Set Z Position
	 */
	public void translate(float x, float y, float z)
	{
		_translation.x = x;
		_translation.y = y;
		_translation.z = z;
	}
	

	/**
	 * Set Translation
	 * @param translate Translation Position
	 */
	public void translate(Vec3D translate)
	{
		_translation.x = translate.x;
		_translation.y = translate.y;
		_translation.z = translate.z;
	}
	

	/**
	 * Set Scale
	 * @param s Set Scale across all axes
	 */
	public void scale(float s)
	{
		_scale.x = s;
		_scale.y = s;
		_scale.z = s;
	}
	
	/**
	 * Set Scale
	 * @param s Set scale for individual axes
	 */
	public void scale(float sx, float sy, float sz)
	{
		_scale.x = sx;
		_scale.y = sy;
		_scale.z = sz;
	}
	
	/**
	 * Set Scale
	 * @param scale Set scale
	 */
	public void scale(Vec3D scale)
	{
		_scale.x = scale.x;
		_scale.y = scale.y;
		_scale.z = scale.z;
	}
	
	
	/**
	 * Set Rotation : Change to yaw pitch roll
	 * @param s
	 */
	public void rotate(float rx, float ry, float rz)
	{
		_rotation.x = rx;
		_rotation.y = ry;
		_rotation.z = rz;
	}
	
	/**
	 * Set Pitch (X-Axis Rotation)
	 * @param pitchRad The angle in radians
	 */
	public void setPitch(float pitchRad) {_rotation.x = pitchRad;}
	
	/**
	 * Set Yaw (Y-Axis Rotation)
	 * @param yawRad The angle in radians
	 */
	public void setYaw(float yawRad) {_rotation.y = yawRad;}
	
	/**
	 * Set Roll (Z-Axis Rotation)
	 * @param rollRad The angle in radians
	 */
	public void setRoll(float rollRad) {_rotation.z = rollRad;}		
	
	/**
	 * Sets teh Zoom level of the camera
	 * @param zoom Zoom level
	 */
	public void setZoom(float zoom){_zoom = zoom;}
	
	
	/**
	 * Gets the X-Axis Translation Component
	 * @return X-Axis Translation
	 */
	public float getTranslationX() {return _translation.x;}
	
	/**
	 * Gets the Y-Axis Translation Component
	 * @return Y-Axis Translation
	 */
	public float getTranslationY() {return _translation.y;}
	
	/**
	 * Gets the Z-Axis Translation Component
	 * @return Z-Axis Translation
	 */
	public float getTranslationZ() {return _translation.z;}		
			
	/**
	 * Gets the X-Axis Scale Component
	 * @return X-Axis Scaling
	 */
	public float getScaleX() {return _scale.x;}
	
	/**
	 * Gets the Y-Axis Scale Component
	 * @return Y-Axis Scaling
	 */
	public float getScaleY() {return _scale.y;}
	
	/**
	 * Gets the Z-Axis Scale Component
	 * @return Z-Axis Scaling
	 */
	public float getScaleZ() {return _scale.z;}
	
	/**
	 * Gets the Pitch
	 * @return The angle in Radians
	 */
	public float getPitch() {return _rotation.x;}
	
	/**
	 * Gets the Yaw
	 * @return The angle in Radians
	 */
	public float getYaw() {return _rotation.y;}
	
	/**
	 * Gets the Roll
	 * @return The angle in Radians
	 */
	public float getRoll() {return _rotation.z;}	
	
	/**
	 * Gets the zoom level of the camera
	 * @return
	 */
	public float getZoom(){return _zoom;}
	

	/**
	 * 
	 * @param projection
	 */
	protected void setProjection(IProjection projection)
	{
		if (projection == null)
			throw new IllegalArgumentException();
		
		_projection = projection;
	}
	
	/**
	 * Sets the View
	 * @param view View to Use
	 */
	protected void setView(IView view)
	{
		if (view == null)
			throw new IllegalArgumentException();
		
		_view = view;
	}
	
	
	
	
	/**
	 * 
	 */
	protected Camera()
	{
//		_viewport = new Viewport();
		
		_translation = new Vec3D();
		_scale = new Vec3D(1, 1, 1);
		_rotation = new Vec3D();
	}

	
	/**
	 * 
	 * @param width
	 * @param height
	 * @param graphics
	 */
	public void setupCamera(int width, int height, PGraphics graphics)
	{
		
		/*
		 * doesn't work on graphics.* drawing functions :'( 
		 */
//		PGL gl = graphics.beginPGL();
//		
//		
//		int l = (int)(width * _viewport.getX());
//		int t =	(int)(height * _viewport.getY());
//		int w =	(int)(width * _viewport.getWidth());
//		int h = (int)(height * _viewport.getHeight());
//				
//				
//		gl.viewport(l, t, w, h);	

//		graphics.endPGL();
		
	
		graphics.beginCamera();
		
		// Set View Matrix
		_view.setViewMatrix(width, height, _zoom, graphics);

		// Scale
		graphics.scale(_scale.x, _scale.y, _scale.z);
		
		// Rotate
		graphics.rotate(-_rotation.z, 0, 0, 1);
		graphics.rotate(-_rotation.x, 1, 0, 0);
		graphics.rotate(-_rotation.y, 0, 1, 0);
		
		// Translate
		graphics.translate(-_translation.x, -_translation.y, -_translation.z);
		
		// Set Projection Matrix
		_projection.setProjectionMatrix(width, height, graphics);
		
		graphics.endCamera();
	}
	
	 
}
