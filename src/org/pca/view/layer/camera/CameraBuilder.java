/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.layer.camera;

import processing.core.PGraphics;
import toxi.geom.Vec3D;

/**
 * Constructs a camera using the Builder Pattern
 * @author Spirit
 * TODO: Add zooming capability to the builder
 */
public class CameraBuilder 
{
	private IProjection _projection = null;
	private IView _view = null;
	
	private Vec3D _scale = null;
	private Vec3D _translate = null;
	
	/**
	 * Creates the camera builder
	 */
	public CameraBuilder()
	{
		
	}
	
	/**
	 * Create Default Camera View Matrix
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder view()
	{
		_view = new IView()
				{
					@Override
					public void setViewMatrix(float width, float height, float zoom, PGraphics g) 
					{
						g.camera();						
					}
				};
				
		return this;
	}
	
	/**
	 * Creates a LookAt View Matrix
	 * @param eyeX Position of Camera
	 * @param eyeY Position of Camera
	 * @param eyeZ Position of Camera
	 * @param targetX Target Position of Camera
	 * @param targetY Target Position of Camera
	 * @param targetZ Target Position of Camera
	 * @param upX Up Direction of Camera
	 * @param upY Up Direction of Camera
	 * @param upZ Up Direction of Camera
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder lookAt(float eyeX, float eyeY, float eyeZ, float targetX, float targetY, float targetZ, float upX, float upY, float upZ)
	{
		_view = new LookAt(eyeX, eyeY, eyeZ, targetX, targetY, targetZ, upX, upY, upZ);
		return this;
	}
	

	
	/**
	 * Create Default Orthographic Matrix
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder ortho()
	{
		_projection = new IProjection()
		{
			@Override
			public void setProjectionMatrix(float width, float height, PGraphics g) 
			{
				g.ortho();
			}
		};
		
		return this;
	}
	
	
	/**
	 * Creates an Orthogonal Projection Matrix
	 * @param left Left Extent of the Projection
	 * @param right Right Extent of the Projection
	 * @param bottom Bottom Extent of the Projection
	 * @param top Top of the Projection
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder ortho(float left, float right, float bottom, float top)
	{
		_projection = new Orthographic(left, right, bottom, top);
		return this;
	}
	
	
	
	/**
	 * Create Default Perspective Matrix
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder persective()
	{
		_projection = new IProjection()
		{
			@Override
			public void setProjectionMatrix(float width, float height, PGraphics g)
			{
				g.perspective();
			}
		};
		return this;
	}
	
	
	/**
	 * Creates a 3D Perspective Matrix
	 * @param fov Field of View
	 * @param near Near Clipping Plane
	 * @param far Far Clipping Plane
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder perspective(float fov, float near, float far)
	{
		_projection = new Perspective(fov, near, far);
		
		return this;
	}
	
	
	
	
	
	
	/**
	 * Set camera scale
	 * @param sx
	 * @param sy
	 * @param sz
	 * @return Reference to itself for object chaining
	 */
	public CameraBuilder scale(float sx, float sy, float sz)
	{
		_scale = new Vec3D(sx, sy, sz);
		
		return this;
	}
	
	
	/**
	 * Set Camera Translation
	 * @param s Reference to itself for object chaining
	 */
	public CameraBuilder translate(float x, float y, float z)
	{
		_translate = new Vec3D(x, y, z);
		
		return this;
	}	
	
	
	
	/**
	 * Creates the Camera
	 * @return Builds the camera object
	 */
	public Camera create()
	{
		// Create Camera Class
		Camera camera = new Camera();
		
		// Create Projection
		if (_projection == null)
		{
			ortho();
		}
		
		// Create View
		if (_view == null)
		{
			view();
		}
		
		// Matrices
		camera.setProjection(_projection);
		camera.setView(_view);
		
		// Scale
		if (_scale != null)
			camera.scale(_scale);
		
		// Translate
		if (_translate != null)
			camera.translate(_translate);
		
		return camera;
	}
}
