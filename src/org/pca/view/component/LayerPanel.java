/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.component;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.pca.view.layer.Layer;

/**
 * Panel to display checkboxes for each layer
 * 
 * @author Kim
 *
 */
public class LayerPanel extends JPanel
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7999822845316835033L;
	private Collection<JCheckBox> _checkBoxes;
    
    /**
     * 
     * @param layers
     */
    public LayerPanel(Collection<Layer> layers)
    {
        _checkBoxes = new ArrayList<JCheckBox>();
        
        if (layers == null)
        {
            _checkBoxes.add(new JCheckBox("Layer 1"));
            _checkBoxes.add(new JCheckBox("Layer 2"));
            _checkBoxes.add(new JCheckBox("Layer 3"));
        }
        else
        {
            for (Layer layer : layers)
            {
                _checkBoxes.add(new JCheckBox(layer.toString()));
            }
        }

        setLayout(new GridLayout(_checkBoxes.size() + 1, 1));
        
        add(new JLabel("Layers"));

        for (JCheckBox checkBox : _checkBoxes)
        {
            add(checkBox);
        }
    }
    
    /**
     * 
     * @return
     */
    public Collection<JCheckBox> getCheckBoxes()
    {
        return _checkBoxes;
    }
    
}
