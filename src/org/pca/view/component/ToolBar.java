/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.component;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.pca.model.IModule;

/**
 * Toolbar to display toggle buttons for each module
 * 
 * @author Kim
 *
 */
public class ToolBar extends JPanel
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1127639622962523270L;
	private Collection<JToggleButton> _buttons;

    /**
     * 
     */
    public ToolBar(Collection<IModule> modules)
    {
        _buttons = new ArrayList<JToggleButton>();

        if (modules == null)
        {
            // Create temporary buttons
            _buttons.add(new JToggleButton("Point Cloud"));
            _buttons.add(new JToggleButton("Voxels"));
            _buttons.add(new JToggleButton("Object Detection"));
        }
        else
        {
            for (IModule module : modules)
            {
                JToggleButton button = new JToggleButton(module.getName());
                _buttons.add(button);
            }
        }

        // Set layout
        setLayout(new GridLayout(1, _buttons.size()));

        // ButtonGroup group = new ButtonGroup();

        for (JToggleButton button : _buttons)
        {
            // group.add(button);
            add(button);
        }

    }
    
    /**
     * 
     * @return
     */
    public Collection<JToggleButton> getToggleButtons()
    {
        return _buttons;
    }

}
