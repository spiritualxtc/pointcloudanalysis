/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.component;

import java.util.ArrayList;
import java.util.Collection;

import org.pca.view.layer.Layer;

import processing.core.PApplet;


/**
 * 
 * @author Spirit
 *
 */
public class ProcessingComponent extends PApplet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3089077812560075446L;

	private Collection<Layer> _layers;

	private Object _lock = null;


	/**
	 * 
	 */
	public ProcessingComponent()
	{
		_layers = new ArrayList<>();

		_lock = new Object();
	}

	
	/**
	 * Specify the Renderer for Processing to Use
	 * 
	 * @return
	 */
//	@Override
//	public String sketchRenderer()
//	{
//		return P3D;
//	}
	
	
	/**
	 * Setup the processing layer
	 */
	@Override
	public void setup()
	{
		size(800, 600, OPENGL);
		frameRate(60);

		System.out.println("PROCESSING: Setup");
		

		// Initialise Overlays
		System.out.println("PROCESSING: Initialise " + _layers.size() + " layers");
		for (Layer layer : _layers)
			layer.setup(this);	
	}
	

	/**
	 * Adds an overlay for Processing to draw
	 * 
	 * @param layer
	 */
	public void addLayer(Layer layer)
	{
		synchronized (_lock)
		{
			_layers.add(layer);
		}
	}
	
	
	/**
	 * Remove an overlay for Processing to draw
	 * 
	 * @param layer
	 */
	public void removeLayer(Layer layer)
	{
		synchronized (_lock)
		{
			_layers.remove(layer);
		}
	}


	/**
	 * Draw the overlays with Processing
	 */
	@Override
	public void draw()
	{
		synchronized (_lock)
		{
			// Draw background colour - hot tip: don't call background in any overlay.draw components :)
			background(25);
		

			// Draw overlays
			for (Layer overlay : _layers)
			{				
				pushMatrix();

				overlay.redraw(this);
			
				popMatrix();
			}
		}
	}

	

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private void drawFramerate()
	{
		// Draw framerate onto screen
		fill(255);
		text(this.frameRate, 20, 20);
		
		// Draw test axis
		stroke(255,0,0);
		line(0,0,0,100,0,0);
		stroke(0,255,0);
		line(0,0,0,0,100,0);
		stroke(0,0,255);
		line(0,0,0,0,0,100);
	}
	
	






}
