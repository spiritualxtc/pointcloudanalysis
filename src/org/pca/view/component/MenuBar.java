/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.component;


import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * 
 * @author Kim
 *
 */
public class MenuBar extends JMenuBar
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1967943881793394884L;
	private JMenu _fileMenu;
    private JMenu _debugMenu;
    
    // Array of menu items for debug menu
//    private Collection<JMenuItem> _debugItems;

    /**
     * 
     */
    public MenuBar()
    {
        _fileMenu = new JMenu("File");
        _debugMenu = new JMenu("Debug");
        
//        _debugItems = new ArrayList<JMenuItem>();
        
        
        add(_fileMenu);
        add(_debugMenu);
    }
    
    /**
     * 
     * @return
     */
    public JMenu getDebugMenu()
    {
        return _debugMenu;
    }
    
    /**
     * 
     * @return
     */
    public JMenu getFileMenu()
    {
        return _fileMenu;
    }
    
    /**
     * 
     * @param menu
     * @param label
     */
    public void addDebugMenuItem(JMenu menu, String label)
    {
        menu.add(new JMenuItem(label));
    }
}
