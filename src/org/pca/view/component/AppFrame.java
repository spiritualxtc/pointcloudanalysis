/*
 * Point Cloud Analysis - Computer Vision for Robotics 
 * Copyright (C) 2015  PCA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.pca.view.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import org.pca.model.IModule;
import org.pca.view.layer.Layer;

/**
 * Main application frame
 * 
 * @author Kim
 *
 */
@SuppressWarnings("serial")
public class AppFrame extends JFrame
{
    private final int MIN_WIDTH = 800;
    private final int MIN_HEIGHT = 600;

    private MenuBar _menuBar;
    private JPanel _mainContent;
    private JPanel _propertiesPanel;
    private ToolBar _toolBar;
    private LayerPanel _layerPanel;

    private ProcessingComponent _component;

    
    /**
     * Setup application frame
     */
    public AppFrame(Collection<IModule> modules, Collection<Layer> layers)
    {
        super("Point Cloud Analyser");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set size to minimum
        setMinimumSize(new Dimension(MIN_WIDTH, MIN_HEIGHT));
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        // Set menubar
        _menuBar = new MenuBar();
        setJMenuBar(_menuBar);

        // Main content
        _mainContent = new JPanel();
        _mainContent.setLayout(new BorderLayout());

        // Processing component
        _component = new ProcessingComponent();
        _mainContent.add(_component, BorderLayout.CENTER);

        // Add toolbar
        // Get modules if available, else null for testing
        _toolBar = new ToolBar(modules);
        _mainContent.add(_toolBar, BorderLayout.NORTH);

        // Properties panel
        _propertiesPanel = new JPanel();
        _propertiesPanel.setLayout(new BorderLayout());
        _propertiesPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLoweredBevelBorder(),
                BorderFactory.createEmptyBorder(10, 20, 10, 20)));

        // Add layer panel
        _layerPanel = new LayerPanel(layers);
        _propertiesPanel.add(_layerPanel, BorderLayout.NORTH);

        add(_mainContent, BorderLayout.CENTER);
        add(_propertiesPanel, BorderLayout.EAST);

        _component.init();

        pack();
        setVisible(true);
    }

    /**
     * 
     * @return
     */
    public ProcessingComponent getProcessing()
    {
        return _component;
    }

    /**
     * 
     * @return
     */
    public Collection<JToggleButton> getToolBarButtons()
    {
        return _toolBar.getToggleButtons();
    }
    
    /**
     * 
     * @return
     */
    public Collection<JCheckBox> getLayerCheckBoxes()
    {
        return _layerPanel.getCheckBoxes();
    }

    /**
     * Temporary main method to check layout
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                new AppFrame(null, null);
            }
        });
    }

}
